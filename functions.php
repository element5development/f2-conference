<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',             // Scripts and stylesheets
  'lib/extras.php',             // Custom functions
  'lib/setup.php',              // Theme setup
  'lib/titles.php',             // Page titles
  'lib/wrapper.php',            // Theme wrapper class
  'lib/customizer.php',         // Theme customizer
  'lib/wp_e5_setup.php',        // Element5 basic setup & whitelabel
  'lib/wp_e5_whitelabel.php',   // Element5 WordPress Admnin Area whitelabel
  'lib/auto_close_acf.php',   	// Element5 WordPress Admnin Area whitelabel
  'lib/post_types.php',         // Custom Post Type Creation
	'lib/post_taxonomies.php',    // Custom Post Taxonomies Creation
	'lib/button_shortcode.php',   // Button Shortcodes
	'lib/acf-relevanssi.php',     // ACF inside relevanssi excerpts
	'lib/acf-menu-item.php',      // Enable/Display ACF menu items
	'lib/excerpt_length.php',     // Custom Excerpt Length
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
