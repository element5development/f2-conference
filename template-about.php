<?php 
/*-------------------------------------------------------------------
    Template Name: About
-------------------------------------------------------------------*/
?>
<?php get_template_part('templates/pages/title'); ?>

<?php 
if(!empty(get_the_content())) { ?>
  <section class="editor-contents">
    <?php get_template_part('templates/pages/default-contents'); ?>
		<?php if( have_rows('leadership_quotes') ): ?>
			<section class="leadership-quotes">
				<?php while ( have_rows('leadership_quotes') ) : the_row(); ?>
					<div class="leadership-quote">
						<blockquote>
							<p><?php the_sub_field('quote'); ?></p>
							<p><?php the_sub_field('name'); ?></p>
						</blockquote>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php the_field('additional_content'); ?>
  </section>
<?php } ?>

<?php if ( have_rows('bios') ) {
  get_template_part('templates/pages/bios');
} ?>

<?php get_template_part('templates/pages/closing'); ?>