/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
        $(document).ready(function() {

          /*----------------------------------------------------------------*\
						SELECT FIELD PLACEHOLDER
					\*----------------------------------------------------------------*/
					$(function () {
						$('select').addClass('has-placeholder');
					});
					$("select").change(function () {
						$(this).removeClass('has-placeholder');
					});

          //Mobile Navigation adding & removing classes
          $('.hamburger').click(function() {
            $('nav.nav-primary').toggleClass('show');
          });

          function actionwindowclick(elem, action) {
            $(window).on('click', function(e) {
              if (!$(elem).is(e.target) && $(elem).has(e.target).length === 0) {
                action();
              }
            });
          }

          //Seach Overlay fade in and out
          $('.search').on('click', function() {
            $('.search-overlay').fadeToggle('2000', 'swing');
            $('.search-overlay > .block').addClass('enter-screen');
          });
          $('.close-search').on('click', function() {
            $('.search-overlay').fadeToggle('2000', 'swing');
            $('.search-overlay > .block').removeClass('enter-screen');
          });

          //Testimony Slick Slider
          $('.testimonies-container > .block').slick({
            centerMode: true,
            centerPadding: '450px',
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 4500,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [{
                breakpoint: 1500,
                settings: {
                  centerPadding: '300px',
                }
              },
              {
                breakpoint: 1300,
                settings: {
                  centerPadding: '300px',
                }
              },
              {
                breakpoint: 1200,
                settings: {
                  centerPadding: '250px',
                }
              },
              {
                breakpoint: 1100,
                settings: {
                  centerPadding: '200px',
                }
              },
              {
                breakpoint: 1024,
                settings: {
                  centerPadding: '150px',
                }
              },
              {
                breakpoint: 850,
                settings: {
                  centerPadding: '75px',
                }
              },
              {
                breakpoint: 768,
                settings: {
                  centerPadding: '0px',
                }
              }
            ]
          });

          //LEADERSHIP QUOTES
          $('.leadership-quotes').slick({
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
          });

          //Partner Slick Slider
          $('.partners-container > .block').slick({
            accessibility: true,
            autoplay: true,
            autoplaySpeed: 3500,
            infinite: true,
            pauseOnHover: false,
            slidesPerRow: 5,
            rows: 2,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [{
                breakpoint: 1024,
                settings: {
                  slidesPerRow: 3,
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesPerRow: 2,
                }
              },
              {
                breakpoint: 515,
                settings: {
                  slidesPerRow: 1,

                }
              }
            ]
          });

          //Media Slick Slider
          $('.media-container > .block').slick({
            accessibility: true,
            autoplay: true,
            autoplaySpeed: 1500,
            arrows: false,
            infinite: true,
            pauseOnHover: false,
            slidesToShow: 6,
            slidesToScroll: 1,
            speed: 1250,
            responsive: [{
                breakpoint: 1024,
                settings: {
                  slidesToShow: 4,
                  slidesToScroll: 2,
                }
              },
              {
                breakpoint: 768,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                }
              }
            ]
          });

          //Gallery Slick Slider
          $('.gallery > .block').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3500,
            arrows: false,
            fade: true,
            asNavFor: '.gallery > nav',
          });
          $('.gallery > nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.gallery > .block',
            dots: false,
            centerMode: false,
            focusOnSelect: true,
            prevArrow: '<button class="slick-prev"></button>',
            nextArrow: '<button class="slick-next"></button>',
            responsive: [{
                breakpoint: 770,
                settings: {
                  slidesToShow: 3
                }
              },
              {
                breakpoint: 515,
                settings: {
                  slidesToShow: 2
                }
              }
            ]
          });

          //ABOUT BIOS
          $('.bio-headshots .headshot.1').on('click', function() {
            $('.bio-summary .editor-contents.2, .bio-summary .editor-contents.3').removeClass('active');
            $('.bio-summary .editor-contents.1').toggleClass('active');
          });
          $('.bio-headshots .headshot.2').on('click', function() {
            $('.bio-summary .editor-contents.1, .bio-summary .editor-contents.3').removeClass('active');
            $('.bio-summary .editor-contents.2').toggleClass('active');
          });
          $('.bio-headshots .headshot.3').on('click', function() {
            $('.bio-summary .editor-contents.2, .bio-summary .editor-contents.1').removeClass('active');
            $('.bio-summary .editor-contents.3').toggleClass('active');
          });

          //TITLE VIDEO LIGHTBOX
          $('.title-video').on('click', function() {
            $('.title-lightbox').addClass('active');
          });
          $('.btn--video').on('click', function() {
            $('.title-lightbox').addClass('active');
          });
          $('.title-lightbox').on('click', function() {
            var src = $(this).find('iframe').attr('src');
            $(this).find('iframe').attr('src', '');
            $(this).find('iframe').attr('src', src);
            $(this).removeClass('active');
          });

          //PARTNER VIDEO LIGHTBOX
          $('.partner-video').on('click', function() {
            var iframe = $(this).siblings('.lightbox').find('iframe');
            $(this).next().addClass('active');
            $(this).closest('article').addClass('active');
            iframe.attr("src", iframe.data("src"));
          });
          $('.lightbox').on('click', function() {
            var iframe = $(this).find('iframe');
            iframe.attr('src', '');
            iframe.attr('src', iframe.data('src'));
            $(this).removeClass('active');
            $(this).closest('article').removeClass('active');
          });

          //SPEAKER VIDEO LIGHTBOX
          $('.speaker-video').on('click', function() {
            var iframe = $(this).parent().siblings('.lightbox').find('iframe');
            $(this).parent().siblings('.lightbox').addClass('active');
            $(this).closest('article').addClass('active');
            iframe.attr("src", iframe.data("src"));
          });
          $('.lightbox').on('click', function() {
            var iframe = $(this).find('iframe');
            iframe.attr('src', '');
            iframe.attr('src', iframe.data('src'));
            $(this).removeClass('active');
            $(this).closest('article').removeClass('active');
          });

          //Entrance Animations
          $(".post-partner.post-preview").viewportChecker({
            classToAdd: "visible animated fadeInScale",
            offset: 200
          });
          $(".post-speaker.post-preview").viewportChecker({
            classToAdd: "visible animated fadeInScale",
            offset: 200
          });
          $(".editor-contents blockquote").addClass('hidden').viewportChecker({
            classToAdd: "visible animated fadeInScale",
            offset: 200
          });
          $(".single-faq").viewportChecker({
            classToAdd: "visible animated fadeIn",
            offset: 200
          });
          $(".thank-yous p").viewportChecker({
            classToAdd: "visible animated fadeIn",
            offset: 200
          });

          //STICKY ITEMS
          var window_width = $(window).width();
          if (window_width < 750) {
            //DISABLE STICKY PARTS ON MOBILE
            $(".sticky-date").trigger("sticky_kit:detach");
            $(".faq-sidebar").trigger("sticky_kit:detach");
          } else {
            //ENABLE STICKY PARTS
            $(".sticky-date").stick_in_parent({
              offset_top: 85
            });
            $(".faq-sidebar").stick_in_parent({
              offset_top: 85
            });
          }
          $(window).resize(function() {
            window_width = $(window).width();
            if (window_width < 750) {
              //DISABLE STICKY PARTS ON MOBILE
              $(".sticky-date").trigger("sticky_kit:detach");
              $(".faq-sidebar").trigger("sticky_kit:detach");
            } else {
              //ENABLE STICKY PARTS
              $(".sticky-date").stick_in_parent({
                offset_top: 85
              });
              $(".faq-sidebar").stick_in_parent({
                offset_top: 85
              });
            }
          });

        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.