var $ = jQuery;
/*------------------------------------------------------------------
	Infinite Scroll on Blog
------------------------------------------------------------------*/
$('.blog-grid-contain').infiniteScroll({
  path: '.next.page-numbers',
  append: '.post-preview',
  button: '.load-more-btn',
  hideNav: '.navigation.pagination',
  scrollThreshold: false,
  checkLastPage: true,
});