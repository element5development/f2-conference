
<?php get_template_part('templates/pages/title'); ?>

<?php if(!empty(get_the_content())) : ?>
  <section class="editor-contents">
  	<?php get_template_part('templates/pages/default-contents'); ?>
  </section>
<?php endif; ?>

<?php if ( is_page(366) ) { //SPEAKERS PAGE
	get_template_part('templates/posts/speakers-preview');
} ?>

<?php if ( is_page(466) ) { //PARTNERS PAGE
	get_template_part('templates/posts/partners-preview');
} ?>

<?php if ( is_page(470) ) { //VENUE PAGE
	get_template_part('templates/posts/venue-preview');
} ?>

<?php get_template_part('templates/pages/closing'); ?>
