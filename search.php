<?php get_template_part('templates/page', 'header'); ?>

<section class="search-contents editor-contents">
	<h1>Search Results</h1>
	<h2>below you will find all your results.</h2>
	<div class="search-feed">
		<?php while (have_posts()) : the_post(); ?>
				<?php get_template_part('templates/posts/search'); ?>
		<?php endwhile; ?>
	</div>
</section>

<?php the_posts_navigation(); ?>
