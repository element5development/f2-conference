<?php

/****************************************************************************
		YEAR
****************************************************************************/
function create_conferenceyear_tax() {

	$labels = array(
		'name'              => _x( 'Conference Year', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Conference Year', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Conference Year', 'textdomain' ),
		'all_items'         => __( 'All Conference Year', 'textdomain' ),
		'parent_item'       => __( 'Parent Conference Year', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Conference Year:', 'textdomain' ),
		'edit_item'         => __( 'Edit Conference Year', 'textdomain' ),
		'update_item'       => __( 'Update Conference Year', 'textdomain' ),
		'add_new_item'      => __( 'Add New Conference Year', 'textdomain' ),
		'new_item_name'     => __( 'New Conference Year Name', 'textdomain' ),
		'menu_name'         => __( 'Conference Year', 'textdomain' ),
	);
	$args = array(
		'labels' => $labels,
		'description' => __( '', 'textdomain' ),
		'hierarchical' => true,
		'public' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_nav_menus' => false,
		'show_in_rest' => false,
		'show_tagcloud' => false,
		'show_in_quick_edit' => true,
		'show_admin_column' => false,
	);
	register_taxonomy( 'conferenceyear', array('days', 'speaker', 'venue', 'partner', 'mediapartner', ), $args );

}
add_action( 'init', 'create_conferenceyear_tax' );

?>