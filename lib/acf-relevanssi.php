<?php
/****************************************************************************
  DISPLAY ACF IN RELEVANSSI EXCERPT
****************************************************************************/
add_filter('relevanssi_excerpt_content', 'custom_fields_to_excerpts', 10, 3);
function custom_fields_to_excerpts($content, $post, $query) {
    $custom_field = get_post_meta($post->ID, 'bio', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'intro', true);
		$content .= " " . $custom_field;
    $custom_field = get_post_meta($post->ID, 'summary', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'description', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'editor_bg_contents', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'questions_answers', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'thanks', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'full_cta_heading', true);
		$content .= " " . $custom_field;
		$custom_field = get_post_meta($post->ID, 'full_cta_description', true);
		$content .= " " . $custom_field;

    return $content;
}