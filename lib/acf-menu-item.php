<?php
/****************************************************************************
  DISPLAY MENU ITEMS
****************************************************************************/
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$arrow = get_field('new_tag', $item);
		if( $arrow ) {
			$item->title .= '<svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14"><path d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path></svg>';
		}
	}
	return $items;
}

?>