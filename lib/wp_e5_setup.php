<?php

/*----------------------------------------------------------------*\
	ENQUEUE JS AND CSS FILES
\*----------------------------------------------------------------*/
function wp_main_assets() {
  wp_enqueue_style( 'style-name', get_stylesheet_uri() );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), '1.0', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), '1.0', true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/****************************************************************************
  ALLOW SVG & ZIP FILES IN WORDPRESS
****************************************************************************/
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['zip'] = 'application/zip';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
function bodhi_svgs_disable_real_mime_check( $data, $file, $filename, $mimes ) {
  $wp_filetype = wp_check_filetype( $filename, $mimes );
  $ext = $wp_filetype['ext'];
  $type = $wp_filetype['type'];
  $proper_filename = $data['proper_filename'];
  return compact( 'ext', 'type', 'proper_filename' );
}
add_filter( 'wp_check_filetype_and_ext', 'bodhi_svgs_disable_real_mime_check', 10, 4 );
/****************************************************************************
  ENABLE ACF OPTIONS PAGE
****************************************************************************/
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
  acf_add_options_sub_page('Theme Options');
  acf_add_options_sub_page('Default Images');
}
/****************************************************************************
  UPDATE GRAVITY FORM SUBMIT INPUT TO BUTTON ELEMENT
****************************************************************************/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="btn btn--primary"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );
/****************************************************************************
  STYLEGUIDE HEX TO RGB
****************************************************************************/
function hex2rgb( $colour ) {
  if ( $colour[0] == '#' ) { 
    $colour = substr( $colour, 1 ); 
  }
  if ( strlen( $colour ) == 6 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
  } elseif ( strlen( $colour ) == 3 ) {
    list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
  } else {
    return false;
  }
  $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
  return 'rgb('.$r.', '.$g.', '.$b.')';
}
/****************************************************************************
  STYLEGUIDE RGB TO CMYK
****************************************************************************/
function hex2rgb2($hex) {
  $color = str_replace('#','',$hex);
  $rgb = array(
    'r' => hexdec(substr($color,0,2)),
    'g' => hexdec(substr($color,2,2)),
    'b' => hexdec(substr($color,4,2)),
  );
  return $rgb;
}
function rgb2cmyk($var1,$g=0,$b=0) {
  if (is_array($var1)) { 
    $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
  } else { 
    $r = $var1; 
  }
  $r = $r / 255; $g = $g / 255; $b = $b / 255;
  $bl = 1 - max(array($r,$g,$b));
  if ( ( 1 - $bl ) > 0 ) {
    $c = ( 1 - $r - $bl ) / ( 1 - $bl ); 
    $m = ( 1 - $g - $bl ) / ( 1 - $bl ); 
    $y = ( 1 - $b - $bl ) / ( 1 - $bl );
  }
  $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
  return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
}




/****************************************************************************
*****************************************************************************



  NEED TO GENERATE MORE ASSESTS? CHECK OUT https://www.wp-hasty.com/



*****************************************************************************
****************************************************************************/
?>