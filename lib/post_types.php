<?php

/****************************************************************************
		CONFERENCE DAYS
****************************************************************************/
function create_days_cpt() {
	$labels = array(
		'name' => __( 'Conference Days', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Conference Days', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Conference Days', 'textdomain' ),
		'name_admin_bar' => __( 'Conference Days', 'textdomain' ),
		'archives' => __( 'Conference Days Archives', 'textdomain' ),
		'attributes' => __( 'Conference Days Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Conference Days:', 'textdomain' ),
		'all_items' => __( 'All Conference Days', 'textdomain' ),
		'add_new_item' => __( 'Add New Conference Days', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Conference Days', 'textdomain' ),
		'edit_item' => __( 'Edit Conference Days', 'textdomain' ),
		'update_item' => __( 'Update Conference Days', 'textdomain' ),
		'view_item' => __( 'View Conference Days', 'textdomain' ),
		'view_items' => __( 'View Conference Days', 'textdomain' ),
		'search_items' => __( 'Search Conference Days', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Conference Days', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Conference Days', 'textdomain' ),
		'items_list' => __( 'Conference Days list', 'textdomain' ),
		'items_list_navigation' => __( 'Conference Days list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Conference Days list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Conference Days', 'textdomain' ),
		'description' => __( ' ', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-calendar',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array('conferenceyear', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'days', $args );
}
add_action( 'init', 'create_days_cpt', 0 );

/****************************************************************************
		SPEAKERS
****************************************************************************/
function create_speaker_cpt() {
	$labels = array(
		'name' => __( 'Speakers', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Speaker', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Speakers', 'textdomain' ),
		'name_admin_bar' => __( 'Speaker', 'textdomain' ),
		'archives' => __( 'Speaker Archives', 'textdomain' ),
		'attributes' => __( 'Speaker Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Speaker:', 'textdomain' ),
		'all_items' => __( 'All Speakers', 'textdomain' ),
		'add_new_item' => __( 'Add New Speaker', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Speaker', 'textdomain' ),
		'edit_item' => __( 'Edit Speaker', 'textdomain' ),
		'update_item' => __( 'Update Speaker', 'textdomain' ),
		'view_item' => __( 'View Speaker', 'textdomain' ),
		'view_items' => __( 'View Speakers', 'textdomain' ),
		'search_items' => __( 'Search Speaker', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Speaker', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Speaker', 'textdomain' ),
		'items_list' => __( 'Speakers list', 'textdomain' ),
		'items_list_navigation' => __( 'Speakers list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Speakers list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Speaker', 'textdomain' ),
		'description' => __( ' ', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-megaphone',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array('conferenceyear', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => true,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'speaker', $args );
}
add_action( 'init', 'create_speaker_cpt', 0 );

/****************************************************************************
		VENUES
****************************************************************************/
function create_venue_cpt() {

	$labels = array(
		'name' => __( 'Venues', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Venue', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Venues', 'textdomain' ),
		'name_admin_bar' => __( 'Venue', 'textdomain' ),
		'archives' => __( 'Venue Archives', 'textdomain' ),
		'attributes' => __( 'Venue Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Venue:', 'textdomain' ),
		'all_items' => __( 'All Venues', 'textdomain' ),
		'add_new_item' => __( 'Add New Venue', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Venue', 'textdomain' ),
		'edit_item' => __( 'Edit Venue', 'textdomain' ),
		'update_item' => __( 'Update Venue', 'textdomain' ),
		'view_item' => __( 'View Venue', 'textdomain' ),
		'view_items' => __( 'View Venues', 'textdomain' ),
		'search_items' => __( 'Search Venue', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Venue', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Venue', 'textdomain' ),
		'items_list' => __( 'Venues list', 'textdomain' ),
		'items_list_navigation' => __( 'Venues list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Venues list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Venue', 'textdomain' ),
		'description' => __( ' ', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-location-alt',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array('conferenceyear', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'venue', $args );
}
add_action( 'init', 'create_venue_cpt', 0 );

/****************************************************************************
		TESTIMONY
****************************************************************************/
function create_testimony_cpt() {
	$labels = array(
		'name' => __( 'Testimonies', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Testimony', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Testimonies', 'textdomain' ),
		'name_admin_bar' => __( 'Testimony', 'textdomain' ),
		'archives' => __( 'Testimony Archives', 'textdomain' ),
		'attributes' => __( 'Testimony Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Testimony:', 'textdomain' ),
		'all_items' => __( 'All Testimonies', 'textdomain' ),
		'add_new_item' => __( 'Add New Testimony', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Testimony', 'textdomain' ),
		'edit_item' => __( 'Edit Testimony', 'textdomain' ),
		'update_item' => __( 'Update Testimony', 'textdomain' ),
		'view_item' => __( 'View Testimony', 'textdomain' ),
		'view_items' => __( 'View Testimonies', 'textdomain' ),
		'search_items' => __( 'Search Testimony', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Testimony', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Testimony', 'textdomain' ),
		'items_list' => __( 'Testimonies list', 'textdomain' ),
		'items_list_navigation' => __( 'Testimonies list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Testimonies list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Testimony', 'textdomain' ),
		'description' => __( ' ', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-format-quote',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'testimony', $args );
}
add_action( 'init', 'create_testimony_cpt', 0 );

/****************************************************************************
		PARTNERS
****************************************************************************/
function create_partner_cpt() {

	$labels = array(
		'name' => __( 'Partners', 'Post Type General Name', 'textdomain' ),
		'singular_name' => __( 'Partner', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => __( 'Partners', 'textdomain' ),
		'name_admin_bar' => __( 'Partner', 'textdomain' ),
		'archives' => __( 'Partner Archives', 'textdomain' ),
		'attributes' => __( 'Partner Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Partner:', 'textdomain' ),
		'all_items' => __( 'All Partners', 'textdomain' ),
		'add_new_item' => __( 'Add New Partner', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Partner', 'textdomain' ),
		'edit_item' => __( 'Edit Partner', 'textdomain' ),
		'update_item' => __( 'Update Partner', 'textdomain' ),
		'view_item' => __( 'View Partner', 'textdomain' ),
		'view_items' => __( 'View Partners', 'textdomain' ),
		'search_items' => __( 'Search Partner', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Partner', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Partner', 'textdomain' ),
		'items_list' => __( 'Partners list', 'textdomain' ),
		'items_list_navigation' => __( 'Partners list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Partners list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Partner', 'textdomain' ),
		'description' => __( ' ', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-heart',
		'supports' => array('title', 'custom-fields', ),
		'taxonomies' => array('conferenceyear', ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => false,
		'can_export' => true,
		'has_archive' => false,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => false,
		'capability_type' => 'post',
	);
	register_post_type( 'partner', $args );

}
add_action( 'init', 'create_partner_cpt', 0 );

?>