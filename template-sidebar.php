<?php 
/*-------------------------------------------------------------------
    Template Name: Sidebar
-------------------------------------------------------------------*/
?>

<?php get_template_part('templates/pages/title'); ?>

<div class="sidebar-content-container">

  <aside class="sidebar">
    <?php get_template_part('templates/pages/sidebar'); ?>
  </aside> 

  <div class="contents">
    <?php 
    if(!empty(get_the_content())) { ?>
      <section class="editor-contents">
        <?php get_template_part('templates/pages/default-contents'); ?>
      </section>
    <?php } ?>
  </div>

</div>

<?php get_template_part('templates/pages/closing'); ?>