<?php get_template_part('templates/page', 'header'); ?>

<section class="error-contents editor-contents">
	<div class="block">
		<h1>404 ERROR</h1>
		<h2>It seems you have found a lost page.</h2>
		<p>You might have better luck if you try searching for what you want.</p>
		<?php echo get_search_form(); ?>
		<p>press enter to search.</p>
	</div>
</section>
