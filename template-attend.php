<?php 
/*-------------------------------------------------------------------
    Template Name: Attend
-------------------------------------------------------------------*/
?>
<?php get_template_part('templates/pages/title'); ?>

<?php 
if(!empty(get_the_content())) { ?>
  <section class="editor-contents">
    <?php get_template_part('templates/pages/default-contents'); ?>
  </section>
<?php } ?>

<?php if ( get_field('editor_bg_contents') ) {
  get_template_part('templates/pages/editor-bg');
} ?>

<?php get_template_part('templates/pages/closing'); ?>