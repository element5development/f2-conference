<?php 
/*-------------------------------------------------------------------
    Template Name: Faq
-------------------------------------------------------------------*/
?>

<?php get_template_part('templates/pages/title'); ?>

<div class="sidebar-content-container">

  <aside class="sidebar faq-sidebar">
    <?php get_template_part('templates/pages/sidebar'); ?>
  </aside> 

  <div class="contents">
    <?php 
    if(!empty(get_the_content())) { ?>
      <section class="editor-contents">
        <?php get_template_part('templates/pages/default-contents'); ?>
      </section>
    <?php } ?>

    <?php if ( have_rows('faqs')  ) {
      get_template_part('templates/pages/faqs');
    } ?>
  </div>

</div>

<?php get_template_part('templates/pages/closing'); ?>