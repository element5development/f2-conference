<?php 
/*-------------------------------------------------------------------
    Template Name: Home
-------------------------------------------------------------------*/
?>

<!-- 
********************************************************************
PAGE TITLE 
********************************************************************
-->
<?php if ( get_field('background_image') ) {
	$background = get_field('background_image'); 
} else {
	$background = get_field('default_page_image', 'options'); 
} ?>
<section class="home-title title-page" style="background-image: url(<?php echo $background['url']; ?>)">
	<div class="title-overlay"></div>
	<div class="video-background">
		<video muted="" autoplay="" loop="" poster="<?php echo $background['url']; ?>" class="bgvid"> 
			<source src="<?php echo get_template_directory_uri(); ?>/dist/images/F2_Video_Home.webm" type="video/webm">
			<source src="<?php echo get_template_directory_uri(); ?>/dist/images/F2_Video_Home.mp4" type="video/mp4">  
		</video> 
	</div>
	<div class="block">
		<h1>FACE to FACE Entertainment Conferences</h1>
		<h2>RELATIONSHIPS IS OUR CURRENCY</h2>
		<?php the_field('intro'); ?>
		<?php 
			$buttonOne = get_field('button_one');
			$buttonTwo = get_field('button_two');
		?>
		<?php if ( $buttonOne ) { ?>
			<a class="btn btn--black" href="<?php echo $buttonOne['url']; ?>" target="<?php echo $buttonOne['target']; ?>"><?php echo $buttonOne['title']; ?></a>
		<?php } ?>
		<?php if ( $buttonTwo ) { ?>
			<a class="btn btn--video" target="_blank"><?php echo $buttonTwo['title']; ?></a>
		<?php } ?>
		<?php if ( get_field('title_video') ) : ?>
			<div class="title-video">
				<svg width="111" height="111" viewBox="0 0 111 111" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" fill-rule="evenodd">
						<path d="M44.8597.329c23.87 0 43.22 19.815 43.22 44.26 0 24.444-19.35 44.26-43.22 44.26-23.87 0-43.22-19.816-43.22-44.26 0-24.445 19.35-44.26 43.22-44.26" fill="#FFD200"/>
						<path d="M39.9696 34.149c-.878.005-1.585.722-1.58 1.6v16.61c-.005.878.702 1.594 1.58 1.6.278-.001.551-.077.79-.22l7.21-4.15 7.11-4.15c.765-.454 1.018-1.441.564-2.206a1.6118 1.6118 0 0 0-.564-.564l-7.11-4.15-7.11-4.15a1.5597 1.5597 0 0 0-.79-.22m1.4 4.14l4.93 2.88 4.94 2.92-4.93 2.88-4.94 2.85v-11.53" fill="#114579"/>
						<path d="M24.3197 57.4286h41.1c-2.035-1.751-2.265-4.819-.514-6.854a4.8706 4.8706 0 0 1 2.134-1.436v-.45c-2.541-.866-3.898-3.628-3.032-6.168a4.8595 4.8595 0 0 1 3.032-3.032v-.45c-2.544-.856-3.913-3.611-3.057-6.155.288-.858.81-1.618 1.506-2.195h-41.099c2.031 1.756 2.254 4.824.498 6.855a4.8551 4.8551 0 0 1-2.118 1.425v.45c2.543.858 3.909 3.615 3.051 6.159a4.8595 4.8595 0 0 1-3.051 3.051v.46c2.542.861 3.906 3.619 3.046 6.161a4.8615 4.8615 0 0 1-1.496 2.179zm45.72 3h-50.27v-4.899h1.5c1.019 0 1.845-.827 1.845-1.846 0-1.019-.826-1.845-1.845-1.845h-1.5v-6h1.5c1.019-.075 1.784-.962 1.709-1.981-.067-.914-.795-1.641-1.709-1.709h-1.5v-6h1.5c1.019-.075 1.784-.962 1.709-1.981-.067-.914-.795-1.641-1.709-1.709h-1.5v-4.78h50.27v4.9h-1.5c-1.019.075-1.784.962-1.709 1.981.068.914.794 1.642 1.709 1.709h1.5v6h-1.5c-1.019 0-1.845.826-1.845 1.845 0 1.019.826 1.845 1.845 1.845h1.5v6h-1.5c-1.019 0-1.845.826-1.845 1.845 0 1.019.826 1.845 1.845 1.845h1.5v4.78z" fill="#114579"/>
						<path fill="#FFF" d="M7.5297 88.2392l-1.32 1.569 3.76 3.17-1.33 1.58-3.78-3.17-1.34 1.59 4.31 3.611-1.33 1.58-6.32-5.3 6.63-7.91 6.2 5.189-1.33 1.58zM17.8397 95.8085l2.42 1.391-4.11 2.089.3 4.89-2.54-1.46v-2.88l-2.65 1.35-2.43-1.4 4.38-2.18-.24-4.619 2.53 1.46v2.639zM26.5194 103.7685c.195-.969-.432-1.913-1.401-2.108-.969-.195-1.913.432-2.108 1.401-.173.859.301 1.717 1.119 2.027.97.322 2.017-.204 2.338-1.174.016-.047.03-.096.042-.146h.01zm2.39.931c-.81 2.279-2.71 3.309-4.81 2.559a2.9405 2.9405 0 0 1-1.91-1.9l-1.32 3.73-2.41-.86 3.62-10.189 2.4.859-.35 1a2.9 2.9 0 0 1 2.62-.27c2.13.71 2.98 2.75 2.16 5.071zM36.6996 105.3388c.205-.791-.27-1.599-1.062-1.804a1.5196 1.5196 0 0 0-.188-.036c-.876-.115-1.696.457-1.89 1.32l3.14.52zm2.19 1.75l-5.52-.91c.007.918.69 1.69 1.6 1.81.705.109 1.425-.068 2-.49l1.11 1.51a4.5722 4.5722 0 0 1-3.65.82c-2.245-.117-3.969-2.032-3.852-4.277.118-2.244 2.032-3.969 4.277-3.852.279.015.555.058.825.129 2.79.43 3.86 2.47 3.21 5.26zM44.2396 103.9784a2.8968 2.8968 0 0 1 2.61-1.409v2.3c-1.47-.161-2.46.509-2.62 1.63l-.08 4.089-2.55-.049.16-8h2.55l-.07 1.439zM49.4696 110.4492l-.7-7.92 2.53-.221.7 7.92-2.53.221zm1.72-10.431c.063.738-.484 1.387-1.221 1.45-.737.063-1.386-.484-1.449-1.221-.063-.734.477-1.38 1.21-1.448.713-.09 1.362.416 1.452 1.128.004.03.007.061.008.091zM59.7098 104.0888c-.105-.811-.848-1.383-1.658-1.278a1.431 1.431 0 0 0-.193.038c-.855.221-1.401 1.057-1.259 1.93l3.11-.69zm2.68.85l-5.42 1.22c.378.812 1.285 1.235 2.15 1a2.6807 2.6807 0 0 0 1.68-1.21l1.599 1a4.5676 4.5676 0 0 1-3.12 2.14c-2.163.611-4.412-.646-5.023-2.81-.611-2.163.646-4.412 2.809-5.023.124-.035.249-.063.374-.087 2.741-.6 4.531.92 4.951 3.77zM71.9696 99.3388l1.93 4.63-2.34 1-1.66-4c-.177-.725-.908-1.168-1.631-.991-.104.025-.204.062-.299.111-.824.341-1.218 1.284-.88 2.11l1.67 3.89-2.35 1-3.07-7.34 2.36-1 .5 1.2c.17-1.061.895-1.95 1.9-2.33 1.399-.658 3.067-.056 3.725 1.343.057.122.106.248.145.377M79.7899 93.0888l-.95 1.92a1.9095 1.9095 0 0 0-1.8.17c-.937.599-1.21 1.844-.61 2.78.599.936 1.843 1.209 2.78.61.582-.329.924-.962.88-1.63l2.149-.08a3.9052 3.9052 0 0 1-2.05 3.5c-1.896 1.202-4.408.637-5.609-1.26-1.201-1.897-.638-4.408 1.26-5.61 1.152-.824 2.655-.975 3.95-.4M86.2294 89.6884c-.49-.654-1.417-.787-2.071-.297-.049.036-.095.075-.139.117-.643.608-.717 1.605-.17 2.3l2.38-2.12zm2.74-.6l-4.179 3.71c.719.543 1.727.484 2.379-.139.532-.481.849-1.155.88-1.87l1.88.089c-.051 1.302-.654 2.522-1.66 3.35-1.519 1.658-4.093 1.771-5.75.253-1.657-1.519-1.77-4.093-.252-5.751.168-.184.353-.352.552-.502 2.07-1.92 4.341-1.46 6.15.86zM91.2196 78.4687l2 1.36 2.64-3.81 1.7 1.17-2.64 3.81 3.05 2.17-1.49 2.16-8.49-5.87 4.36-6.3 1.7 1.17zM98.3895 66.539c1 .479 1.72 1.72 2.29 3.22l1.13 2.84 1.851-3.94 1.879.88-3.28 7-1.54-.72-2-5a3.137 3.137 0 0 0-1.18-1.65c-.53-.25-1 0-1.35.66a4.734 4.734 0 0 0 0 3.109l-2.13.091a7.0108 7.0108 0 0 1 .37-4.45c.89-1.851 2.481-2.73 3.96-2.04M99.4598 61.4687l2.3.63 1.21-4.46 2 .55-1.231 4.46 3.671 1-.7 2.53-10-2.75 2-7.38 2 .55zM101.3495 52.169l2 .19.45-4.91 2.06.19-.449 4.91 2.069.19.52-5.6 2.06.19-.76 8.21-10.29-1 .74-8.05 2.06.19zM100.5897 35.3686l1.93 1.35a3.5268 3.5268 0 0 0-1.1 2.71c.154 1.657 1.623 2.875 3.28 2.72 1.657-.155 2.874-1.623 2.72-3.28a3.8718 3.8718 0 0 0-1.45-2.48l1.47-1.68a6.243 6.243 0 0 1 2.28 4.07c.307 2.895-1.79 5.49-4.685 5.797-2.894.307-5.489-1.791-5.797-4.685a8.2955 8.2955 0 0 1-.018-.222 6.0016 6.0016 0 0 1 1.37-4.3"/>
					</g>
				</svg>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php if ( get_field('title_video') ) : ?>
	<div class="title-lightbox">
		<div class="contents">
			<svg viewBox="0 0 75 75" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
				<path d="M20.1 55.9c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6l14.6-14.6 14.6 14.6c.4.4.9.6 1.4.6.5 0 1-.2 1.4-.6.8-.8.8-2 0-2.8L40.3 38.5l14.6-14.6c.8-.8.8-2 0-2.8-.8-.8-2-.8-2.8 0L37.5 35.7 22.9 21.1c-.8-.8-2-.8-2.8 0-.8.8-.8 2 0 2.8l14.6 14.6-14.6 14.6c-.8.8-.8 2 0 2.8z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
				<path d="M59.5 73.5c7.7 0 14-6.3 14-14v-44c0-7.7-6.3-14-14-14h-44c-7.7 0-14 6.3-14 14v44c0 7.7 6.3 14 14 14h44zm-54-14v-44c0-5.5 4.5-10 10-10h44c5.5 0 10 4.5 10 10v44c0 5.5-4.5 10-10 10h-44c-5.5 0-10-4.5-10-10z" fill="#fff" fill-rule="nonzero" stroke="#fff"></path>
			</svg>
			<?php the_field('title_video'); ?>
		</div>
	</div>
<?php endif; ?>

<!-- 
********************************************************************
COUNTDOWN 
********************************************************************
-->
<?php 
	$now = date('m/d/Y H:i:s');
	$nowtoday = date( "m/d/Y H:i:s", strtotime( "$now - 4 hours" ) );
	$today = DateTime::createFromFormat('m/d/Y H:i:s', $nowtoday);
	$conf_date = DateTime::createFromFormat('m/d/Y H:i:s', get_field('countdown'));
	$interval = date_diff($conf_date,$today);
	$week_total = floor($interval->format('%a')/7);
	$day_total = $interval->format('%a')%7;
	$hour_total = $interval->format('%H');
	$min_total = $interval->format('%i');
	$week_graph = 220 - (($week_total / 52) * 220); 
	$day_graph = 220 - (($day_total / 7) * 220);
	$hour_graph = 220 - (($hour_total / 24) * 220);
	$min_graph = 220 - (($min_total / 60) * 220);
	$display_date = date("F d, Y",strtotime(get_field('countdown')));
	$A = strtotime($nowtoday);
	$B = strtotime(get_field('countdown'));
?>
<?php if ( $A < $B ) { ?>
	<section class="conf-countdown">
		<div class="block">
			<p><?php the_field('countdown_title'); ?> <?php echo $display_date; ?>:</p>
			<div class="count-down">
				<div class="block">
					<svg height="74" width="74" class="no-show">
						<defs>
							<linearGradient id="linear" x1="0%" y1="0%" x2="100%" y2="0%">
								<stop offset="0%"   stop-color="#FFD200"/>
								<stop offset="100%" stop-color="#FD7315"/>
							</linearGradient>
						</defs>
						<circle cx="37" cy="37" r="35" stroke="#FFD200" stroke-width="3" fill="none" />
						<circle id="graph" cx="37" cy="37" r="35" stroke="url(#linear)" stroke-width="3" stroke-dasharray="220" stroke-dashoffset="<?php echo $week_graph ?>" transform="rotate(-90 37 37)" fill="none" />
						<text x="50%" y="50%" dy="8" text-anchor="middle"><?php echo $week_total; ?></text>
					</svg>
					<p>weeks</p>
				</div>
				<div class="block">
					<svg height="74" width="74" class="no-show">
						<circle cx="37" cy="37" r="35" stroke="#FFD200" stroke-width="3" fill="none" />
						<circle id="graph" cx="37" cy="37" r="35" stroke="url(#linear)" stroke-width="3" stroke-dasharray="220" stroke-dashoffset="<?php echo $day_graph ?>" transform="rotate(-90 37 37)" fill="none" />
						<text x="50%" y="50%" dy="8" text-anchor="middle"><?php echo $day_total; ?></text>
					</svg>
					<p>days</p>
				</div>
				<div class="block">
					<svg height="74" width="74" class="no-show">
						<circle cx="37" cy="37" r="35" stroke="#FFD200" stroke-width="3" fill="none" />
						<circle id="graph" cx="37" cy="37" r="35" stroke="url(#linear)" stroke-width="3" stroke-dasharray="220" stroke-dashoffset="<?php echo $hour_graph ?>" transform="rotate(-90 37 37)" fill="none" />
						<text x="50%" y="50%" dy="8" text-anchor="middle"><?php echo $hour_total; ?></text>
					</svg>
					<p>hours</p>
				</div>
				<div class="block">
					<svg height="74" width="74" class="no-show">
						<circle cx="37" cy="37" r="35" stroke="#FFD200" stroke-width="3" fill="none" />
						<circle id="graph" cx="37" cy="37" r="35" stroke="url(#linear)" stroke-width="3" stroke-dasharray="220" stroke-dashoffset="<?php echo $min_graph ?>" transform="rotate(-90 37 37)" fill="none" />
						<text x="50%" y="50%" dy="8" text-anchor="middle"><?php echo $min_total; ?></text>
					</svg>
					<p>min</p>
				</div>
			</div>

		</div>
	</section>
<?php } ?>

<!-- 
********************************************************************
OPTIONAL 1-4 CARDS
********************************************************************
-->
<?php if( have_rows('optional_cards') ): ?> 
	<?php $count = count(get_field('optional_cards')); ?>
	<section class="optional-cards count-<?php echo $count; ?>">
		<h2><?php the_field('optional_cards_title'); ?></h2>
		<div>
			<?php while ( have_rows('optional_cards') ) : the_row(); ?>
				<?php if ( get_sub_field('card_type') == 'image' ) : ?>
					<?php $image = get_sub_field('image'); ?>
					<div class="image card">
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php elseif ( get_sub_field('card_type') == 'video' ) : ?>
					<div class="video card">
						<iframe src="https://www.youtube.com/embed/<?php the_sub_field('video'); ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				<?php elseif ( get_sub_field('card_type') == 'text' ) : ?>
					<div class="text card">
							<?php the_sub_field('text'); ?>
					</div>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>

<!-- 
********************************************************************
CALL TO ACTIONS
********************************************************************
-->
<section class="ctas">
		<h2>The F2FEC Experience</h2>
		<?php if( have_rows('ctas') ):
				while ( have_rows('ctas') ) : the_row();
						if( get_row_layout() == 'cta' ): ?>
							<div class="cta">
								<?php $icon = get_sub_field('icon'); ?>
								<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
								<h2><?php the_sub_field('title'); ?></h2>
								<p><?php the_sub_field('summary'); ?></p>
								<?php $button = get_sub_field('button'); ?>
								<a class="btn" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">GATHERING<br><?php echo $button['title']; ?></a>
							</div>
						<?php endif;
				endwhile;
		endif; ?>
</section>

<!-- 
********************************************************************
THEME INVITE
********************************************************************
-->
<?php get_template_part('templates/footer/theme-invite');  ?>

<!-- 
********************************************************************
PARTNERS
********************************************************************
-->
<?php get_template_part('templates/element/slider-partner'); ?>

<!-- 
********************************************************************
TESTIMONIES
********************************************************************
-->
<?php get_template_part('templates/element/slider-testimony'); ?>

<!-- 
********************************************************************
MINI GALLERY
********************************************************************
-->
<section class="mini-gallery">
	<?php 
	$images = get_field('mini_gallery');
	$size = 'medium';
	if( $images ): ?>
			<ul>
					<?php foreach( $images as $image ): ?>
							<li style="background-image: url(<?php echo $image['sizes']['medium']; ?>);">
							</li>
					<?php endforeach; ?>
			</ul>
	<?php endif; ?>
</section>


<?php get_template_part('templates/pages/closing'); ?>