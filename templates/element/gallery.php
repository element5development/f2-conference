<section class="gallery">
	<div class="block">
		<?php $images = get_field('gallery');	?>
		<?php if( $images ): ?>
      <?php foreach( $images as $image ): ?>
          <div class="image-slide" style="background-image: url('<?php echo $image[url]; ?>')"></div>
      <?php endforeach; ?>
		<?php endif; ?>
	</div>
	<nav>
		<?php 
			$images = get_field('gallery');	
			$size = 'thumbnail';
		?>
		<?php if( $images ): ?>
      <?php foreach( $images as $image ): ?>
          <div class="nav-slide"><div class="image" style="background-image: url('<?php echo $image['sizes']['thumbnail']; ?>')"></div></div>
      <?php endforeach; ?>
		<?php endif; ?>
	</nav>
</section>