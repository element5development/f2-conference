<?php 
	$background = get_field('full_cta_background'); 
	$link = get_field('full_cta_button_link');
	$link2 = get_field('full_cta_button_link_2');
	$link3 = get_field('full_cta_button_link_3');
?>
<section class="full-cta" style="background-image: url(<?php echo $background['url']; ?>)">
	<div class="title-overlay"></div>
	<div class="block">
		<h2><?php the_field('full_cta_heading'); ?></h2>
		<p><?php the_field('full_cta_description'); ?></p>
		<div class="block">
			<?php if ( get_field('full_cta_button_link') ) { ?>
				<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="btn"><?php echo $link['title']; ?></a>
			<?php } ?>
			<?php if ( get_field('full_cta_button_link_2') ) { ?>
				<a href="<?php echo $link2['url']; ?>" target="<?php echo $link2['target']; ?>" class="btn"><?php echo $link2['title']; ?></a>
			<?php } ?>
			<?php if ( get_field('full_cta_button_link_3') ) { ?>
				<a href="<?php echo $link3['url']; ?>" target="<?php echo $link3['target']; ?>" class="btn"><?php echo $link3['title']; ?></a>
			<?php } ?>
		</div>
	</div>
</section>