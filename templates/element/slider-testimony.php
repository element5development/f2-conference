<section class="testimonies-container">
	<h2>Experience. Passion. Opportunity.<span>™</span></h2>
	<div class="block">
		<?php
			$args = array(
				'post_type' => array('testimony'),
				'posts_per_page' => -1,
				'nopaging' => true,
				'ignore_sticky_posts' => true,
				'order' => 'DESC',
			);
			$testimonies = new WP_Query( $args );
		?>
		<?php if ( $testimonies->have_posts() ) {
			while ( $testimonies->have_posts() ) { $testimonies->the_post(); ?>
					<div class="testimony-slide">
						<blockquote>
							<p><?php the_field('quote'); ?></p>
							<p><span><?php the_field('name'); ?></span> - <?php the_field('position'); ?></p>
						</blockquote>
					</div>
			<?php }
		}
		wp_reset_postdata();
		?>
	</div>
</section>