<section class="partners-container">
	<h2>Our Partners</h2>
	<a href="<?php echo get_permalink(466); ?>">
		See all partners
		<svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14">
			<path d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path>
		</svg>
	</a>
	<hr>
	<div class="block">
		<?php 
			if ( date('n') > 2 ) {
				$activeyear = date('Y') + 1;
			} else {
				$activeyear = date('Y');
			}
			$args = array(
				'post_type' => array('partner'),
				'posts_per_page' => -1,
				'nopaging' => true,
				'ignore_sticky_posts' => true,
				'orderby'=> 'title',
				'order' => 'ASC',
				'meta_key' => 'media',
				'meta_value' => 'no',
				'tax_query' => array(
					array(
						'taxonomy' => 'conferenceyear',
						'field' => 'name',
						'terms' => array($activeyear),
						'operator' => 'IN',
					),
				),
			);
			$partners = new WP_Query( $args );
		?>
		<?php if ( $partners->have_posts() ) {
			while ( $partners->have_posts() ) { $partners->the_post(); ?>
				<div class="single-slide">
					<div class="partner-slide">	
						<?php if ( get_field('logo_greyscale') ) { ?>
							<?php $logo = get_field('logo_greyscale'); ?>
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php } else { ?>
							<?php $logo = get_field('logo'); ?>
							<img class="greyscale" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		<?php } else { ?>
			<?php 
				$args = array(
					'post_type' => array('partner'),
					'posts_per_page' => -1,
					'nopaging' => true,
					'ignore_sticky_posts' => true,
					'order' => 'DESC',
					'meta_key' => 'media',
					'meta_value' => 'no',
					'tax_query' => array(
						array(
							'taxonomy' => 'conferenceyear',
							'field' => 'name',
							'terms' => array($activeyear -1),
							'operator' => 'IN',
						),
					),
				);
				$partners = new WP_Query( $args );
			?>
			<?php if ( $partners->have_posts() ) {
				while ( $partners->have_posts() ) { $partners->the_post(); ?>
					<div class="single-slide">
						<div class="partner-slide">	
							<?php if ( get_field('logo_greyscale') ) { ?>
								<?php 
									$logo = get_field('logo_greyscale'); 
									$logoURL = $logo['sizes']['medium'];
								?>
								<a target="_blank" href="<?php the_field('website'); ?>">
									<img src="<?php echo $logoURL; ?>" alt="<?php echo $logo['alt']; ?>" />
								</a>
							<?php } else { ?>
								<?php 
									$logo = get_field('logo'); 
									$logoURL = $logo['sizes']['medium'];
								?>
								<a target="_blank" href="<?php the_field('website'); ?>">
									<img class="greyscale" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
								</a>
							<?php } ?>
						</div>
					</div>
				<?php }
			} ?>
		<?php }
		wp_reset_postdata(); ?>
	</div>
</section>