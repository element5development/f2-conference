<section class="cta-card-container">
<?php 
	if( get_field('cta_cards') ):
		while ( has_sub_field('cta_cards') ) : ?>
			<?php $link = get_sub_field('button_link'); ?>
			<a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="cta-card">
				<?php $icon = get_sub_field('icon'); ?>
				<img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
				<h3><?php the_sub_field('heading'); ?></h3>
				<p><?php the_sub_field('description'); ?></p>
				<div class="btn btn--ghost"><span><?php echo $link['title']; ?></span></div>
			</a>
		<?php endwhile;
	endif;
?>
</section>