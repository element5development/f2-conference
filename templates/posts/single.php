<section class="post-title">
	<nav class="breadcrumbs">
		<a href="/news">News</a> /
		<?php $categories = get_the_category(); ?>
		<?php if ( ! empty( $categories ) ) : ?>
			<?php  foreach( $categories as $category ) : ?>
				<a href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo $category->name; ?></a>
			<?php endforeach; ?>
		<?php endif; ?>
	</nav>
	<h1><?php the_title(); ?></h1>
</section>
<section class="meta <?php if ( !get_field('featured_image') ) : ?>has-no-image<?php endif; ?>">
	<p>Published: <?php the_time('F j, Y') ?></p>
	<div class="social-share">
		<p>Share:</p>
		<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_permalink(); ?>">Linkedin</a>
		<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>">Facebook</a>
		<a target="_blank" href="https://twitter.com/share?url=<?php echo get_permalink(); ?>">Twitter</a>
		<a target="_blank" href="mailto:info@example.com?&subject=Check out this article from F2&body=<?php echo get_permalink(); ?>">Email</a>
	</div>
	<?php if ( get_field('featured_image') ) : ?>
		<?php $image = get_field('featured_image'); ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	<?php endif; ?>
</section>

<?php if(!empty(get_the_content())) : ?>
  <section class="editor-contents">
  	<?php get_template_part('templates/pages/default-contents'); ?>
  </section>
<?php endif; ?>