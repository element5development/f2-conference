<?php //DETERMINE YEAR TO DISPLAY
	$activeyear = get_field('active_year', 'options');
?>
<?php $args = array(
	'post_type' => array('partner'),
	'posts_per_page' => -1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'orderby'=> 'title',
	'order' => 'ASC',
	'meta_key' => 'media',
	'meta_value' => 'no',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
);
$partner = new WP_Query( $args ); ?>
<?php if ( $partner->have_posts() ) { ?>
	<section class="partners-container">
		<?php while ( $partner->have_posts() ) { $partner->the_post(); ?>
			<article class="hidden post-partner post-preview cta-card">
				<?php 
					$logo = get_field('logo'); 
					$logoURL = $logo['sizes']['medium'];
				?>
				<a target="_blank" href="<?php the_field('website'); ?>" class="partner-logo">
					<img src="<?php echo $logoURL; ?>" alt="<?php echo $logo['alt'] ?>" />
				</a>
				<p class="partner-intro"><?php the_field('description'); ?></p>
				<p class="partner-contact"><?php the_field('representative_names'); ?></p>
				<div class="partner-video">
					<?php if( have_rows('videos') ) {
						while ( have_rows('videos') ) : the_row();
							if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
								<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
									<g stroke="#35A7FF" stroke-width="2" fill="none">
										<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
										<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
										<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
									</g>
								</svg>
							<?php endif;
						endwhile; ?>
					<?php } ?>
				</div> 
				<?php if( have_rows('videos') ) {
					while ( have_rows('videos') ) : the_row();
						if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
							<div class="lightbox">
								<div class="contents">
									<?php the_sub_field('video'); ?>
								</div>
							</div>
						<?php endif;
					endwhile;
				} ?>
			</article>
		<?php } ?>
	</section>
<?php }	else { ?>
	<section class="no-speakers no-content">
		<div class="block">
			<?php the_field('no_content'); ?>
		</div>
	</section>
	<?php $args = array(
		'post_type' => array('partner'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'orderby'=> 'title',
		'order' => 'ASC',
		'meta_key' => 'media',
		'meta_value' => 'no',
		'tax_query' => array(
			array(
				'taxonomy' => 'conferenceyear',
				'field' => 'name',
				'terms' => array($activeyear - 1),
				'operator' => 'IN',
			),
		),
	);
	$partner = new WP_Query( $args ); ?>
	<?php if ( $partner->have_posts() ) { $i == 0; ?>
		<div class="headline-block">
			<h3><?php echo ($activeyear - 1); ?> Partners</h3>
		</div>
		<section class="partners-container">
			<?php while ( $partner->have_posts() ) { $partner->the_post(); $i = get_the_ID(); ?>
				<article class="hidden post-partner post-preview cta-card">
					<?php 
						$logo = get_field('logo'); 
						$logoURL = $logo['sizes']['medium'];
					?>
					<a target="_blank" href="<?php the_field('website'); ?>" class="partner-logo">
						<img src="<?php echo $logoURL; ?>" alt="<?php echo $logo['alt'] ?>" />
					</a>
					<p class="partner-intro"><?php the_field('description'); ?></p>
					<p class="partner-contact"><?php the_field('representative_names'); ?></p>
					<div class="partner-video">
						<?php if( have_rows('videos') ) {
							while ( have_rows('videos') ) : the_row();
								if ( get_sub_field('year') == ($activeyear - 1) && get_sub_field('video') ) : ?>
									<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
										<g stroke="#35A7FF" stroke-width="2" fill="none">
											<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
											<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
											<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
										</g>
									</svg>
								<?php endif;
							endwhile; ?>
						<?php } ?>
					</div> 
					<?php if( have_rows('videos') ) {
						while ( have_rows('videos') ) : the_row();
							if ( get_sub_field('year') == ($activeyear - 1) && get_sub_field('video') ) : ?>
								<div class="lightbox">
									<div class="contents">
										<?php the_sub_field('video'); ?>
									</div>
								</div>
							<?php endif;
						endwhile;
					} ?>
				</article>
			<?php } ?>
		</section>
	<?php } ?>
<?php } ?>
<?php wp_reset_postdata(); ?>