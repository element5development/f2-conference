<?php //DETERMINE YEAR TO DISPLAY
	$activeyear = get_field('active_year', 'options');
?>
<?php $args = array(
	'post_type' => array('speaker'),
	'posts_per_page' => -1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'meta_key'	=> 'last_name',
	'orderby'	=> 'meta_value',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
);
$speakers = new WP_Query( $args ); ?>
<?php if ( $speakers->have_posts() ) { ?>
	<section class="speakers-container">
		<?php while ( $speakers->have_posts() ) { $speakers->the_post(); ?>
			<article class="hidden post-speaker post-preview cta-card">
				<?php 
					$headshot = get_field('headshot'); 
					$headshotURL = $headshot['sizes']['medium'];
				?>
				<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-headshot" style="background-image: url('<?php echo $headshot['url'] ?>');">
					<div class="overlay"></div>
				</a>
				<a target="_blank" href="<?php the_field('company_website'); ?>">
					<h2 class="speaker-name"><?php the_field('name'); ?> <?php the_field('last_name'); ?></h2>
				</a>
				<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-company">
					<h3><?php the_field('company'); ?></h3>
				</a>
				<p class="speaker-intro">
					<?php if( have_rows('topics') ) :
						while ( have_rows('topics') ) : the_row();
							if ( get_sub_field('year') == $activeyear ) :
								the_sub_field('topic');
							endif;
						endwhile;
					endif; ?>
				</p>
				<div class="speaker-contact">
				<?php if ( get_field('linkedin') ) { ?>
					<a target="_blank" href="<?php the_field('linkedin') ?>" class="social-link">
						<svg xmlns="http://www.w3.org/2000/svg" width="54" height="54" viewBox="0 0 54 54">
							<defs>
								<linearGradient id="f" x1="158.66" x2="196.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
									<stop offset="0" stop-color="#FD7315"></stop>
									<stop offset="1" stop-color="#FFD200"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#f)" d="M158.34 1010.82h8.1v24.39h-8.1zm7.38-4.53a4.6 4.6 0 0 1-3.33 1.23h-.06c-1.32 0-2.4-.42-3.24-1.23a3.99 3.99 0 0 1-1.26-3c0-1.23.45-2.22 1.29-3.03a4.82 4.82 0 0 1 3.33-1.2c1.35 0 2.43.42 3.27 1.2.81.81 1.23 1.8 1.26 3.03 0 1.2-.42 2.19-1.26 3zm21.84 15.87c0-3.66-1.38-5.52-4.08-5.52-1.05 0-1.92.3-2.61.87a5.22 5.22 0 0 0-1.59 2.1 6.18 6.18 0 0 0-.24 1.95v13.65h-8.13c.09-14.73.09-22.86 0-24.39h8.13v3.45a8.16 8.16 0 0 1 7.29-4.02 8.8 8.8 0 0 1 6.78 2.79c1.71 1.83 2.55 4.56 2.55 8.19v13.98h-8.1zm13.47-29.19a9.77 9.77 0 0 0-7.14-2.97h-33.78c-2.76 0-5.16.99-7.14 2.97a9.77 9.77 0 0 0-2.97 7.14v33.78c0 2.76.99 5.16 2.97 7.14a9.77 9.77 0 0 0 7.14 2.97h33.78c2.76 0 5.16-.99 7.14-2.97a9.77 9.77 0 0 0 2.97-7.14v-33.78c0-2.76-.99-5.16-2.97-7.14z" transform="translate(-150 -990)"></path>
						</svg>
					</a>
				<?php } ?>
				<?php if( have_rows('topics') ) {
					while ( have_rows('topics') ) : the_row();
						if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
							<div class="social-link">
								<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
									<g stroke="#FFD200" stroke-width="2" fill="none">
										<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
										<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
										<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
									</g>
								</svg>
							</div>
						<?php endif;
					endwhile; ?>
				<?php } ?>
				<?php if ( get_field('email') ) { ?>
					<a target="_blank" href="mailto:<?php the_field('email'); ?>" class="social-link">
						<svg xmlns="http://www.w3.org/2000/svg" width="69" height="54" viewBox="0 0 69 54">
							<defs>
								<linearGradient id="g" x1="452.07" x2="500.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
									<stop offset="0" stop-color="#FD7315"></stop>
									<stop offset="1" stop-color="#FFD200"></stop>
								</linearGradient>
							</defs>
							<path fill="url(#g)" d="M509.73 1007.4v30.45c0 1.71-.6 3.15-1.8 4.35a5.91 5.91 0 0 1-4.35 1.8h-56.43c-1.71 0-3.15-.6-4.35-1.8a5.91 5.91 0 0 1-1.8-4.35v-30.45a21.93 21.93 0 0 0 3.87 3.36 644.43 644.43 0 0 1 19.05 13.23c1.47 1.05 2.64 1.89 3.57 2.49.9.6 2.1 1.23 3.63 1.86 1.5.63 2.91.93 4.2.93h.09c1.29 0 2.7-.3 4.2-.93a21.1 21.1 0 0 0 3.63-1.86c.9-.6 2.1-1.44 3.54-2.49a813.1 813.1 0 0 1 19.11-13.23 22.03 22.03 0 0 0 3.84-3.36zm-1.83-15.6a5.9 5.9 0 0 0-4.32-1.8h-56.43c-1.98 0-3.51.66-4.56 1.98a7.88 7.88 0 0 0-1.59 5.01c0 1.59.69 3.36 2.1 5.22a19.03 19.03 0 0 0 4.5 4.44c.87.6 3.48 2.43 7.86 5.46 4.38 3.03 7.71 5.37 10.05 6.99.24.18.81.57 1.62 1.17.84.6 1.53 1.11 2.07 1.47.57.36 1.23.78 2.01 1.23.78.48 1.5.81 2.19 1.05s1.35.33 1.92.33h.09c.57 0 1.23-.09 1.92-.33.69-.24 1.41-.57 2.19-1.05.78-.45 1.44-.87 2.01-1.23.54-.36 1.23-.87 2.07-1.47.81-.6 1.35-.99 1.62-1.17l17.94-12.45a17.9 17.9 0 0 0 4.68-4.71 10.16 10.16 0 0 0 1.89-5.79c0-1.71-.6-3.15-1.83-4.35z" transform="translate(-441 -990)"></path>
						</svg>
					</a> 
				<?php } ?>
				<?php if( have_rows('topics') ) {
					while ( have_rows('topics') ) : the_row();
						if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
							<div class="lightbox">
								<div class="contents">
									<?php the_sub_field('video'); ?>
								</div>
							</div>
						<?php endif;
					endwhile;
				} ?>
			</div>
			</article>
		<?php } ?>
	</section>
<?php }	else { ?>
	<section class="no-speakers no-content">
		<div class="block">
			<?php the_field('no_content'); ?>
		</div>
	</section>
	<?php $args = array(
		'post_type' => array('speaker'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'meta_key'	=> 'last_name',
		'orderby'	=> 'meta_value',
		'order' => 'ASC',
		'tax_query' => array(
			array(
				'taxonomy' => 'conferenceyear',
				'field' => 'name',
				'terms' => array($activeyear - 1),
				'operator' => 'IN',
			),
		),
	);
	$speakers = new WP_Query( $args ); ?>
	<?php if ( $speakers->have_posts() ) { ?>
		<div class="headline-block">
			<h3><?php echo ($activeyear - 1); ?> Speakers</h3>
		</div>
		<section class="speakers-container">
			<?php while ( $speakers->have_posts() ) { $speakers->the_post(); ?>
				<article class="hidden post-speaker post-preview cta-card">
					<?php 
						$headshot = get_field('headshot'); 
						$headshotURL = $headshot['sizes']['medium'];
					?>
					<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-headshot" style="background-image: url('<?php echo $headshotURL; ?>');">
						<div class="overlay"></div>
					</a>
					<a target="_blank" href="<?php the_field('company_website'); ?>">
						<h2 class="speaker-name"><?php the_field('name'); ?> <?php the_field('last_name'); ?></h2>
					</a>
					<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-company">
						<h3><?php the_field('company'); ?></h3>
					</a>
					<p class="speaker-intro">
						<?php if( have_rows('topics') ) {
							while ( have_rows('topics') ) : the_row();
								if ( get_sub_field('year') == ($activeyear - 1) ) :
									the_sub_field('topic');
								endif;
							endwhile;
						} ?>
					</p>
					<div class="speaker-contact">
						<?php if ( get_field('linkedin') ) { ?>
							<a target="_blank" href="<?php the_field('linkedin') ?>" class="social-link">
								<svg xmlns="http://www.w3.org/2000/svg" width="54" height="54" viewBox="0 0 54 54">
									<defs>
										<linearGradient id="f" x1="158.66" x2="196.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
											<stop offset="0" stop-color="#FD7315"></stop>
											<stop offset="1" stop-color="#FFD200"></stop>
										</linearGradient>
									</defs>
									<path fill="url(#f)" d="M158.34 1010.82h8.1v24.39h-8.1zm7.38-4.53a4.6 4.6 0 0 1-3.33 1.23h-.06c-1.32 0-2.4-.42-3.24-1.23a3.99 3.99 0 0 1-1.26-3c0-1.23.45-2.22 1.29-3.03a4.82 4.82 0 0 1 3.33-1.2c1.35 0 2.43.42 3.27 1.2.81.81 1.23 1.8 1.26 3.03 0 1.2-.42 2.19-1.26 3zm21.84 15.87c0-3.66-1.38-5.52-4.08-5.52-1.05 0-1.92.3-2.61.87a5.22 5.22 0 0 0-1.59 2.1 6.18 6.18 0 0 0-.24 1.95v13.65h-8.13c.09-14.73.09-22.86 0-24.39h8.13v3.45a8.16 8.16 0 0 1 7.29-4.02 8.8 8.8 0 0 1 6.78 2.79c1.71 1.83 2.55 4.56 2.55 8.19v13.98h-8.1zm13.47-29.19a9.77 9.77 0 0 0-7.14-2.97h-33.78c-2.76 0-5.16.99-7.14 2.97a9.77 9.77 0 0 0-2.97 7.14v33.78c0 2.76.99 5.16 2.97 7.14a9.77 9.77 0 0 0 7.14 2.97h33.78c2.76 0 5.16-.99 7.14-2.97a9.77 9.77 0 0 0 2.97-7.14v-33.78c0-2.76-.99-5.16-2.97-7.14z" transform="translate(-150 -990)"></path>
								</svg>
							</a>
						<?php } ?>
						<?php if( have_rows('topics') ) {
							while ( have_rows('topics') ) : the_row();
								if ( get_sub_field('year') == ($activeyear - 1) && get_sub_field('video') ) : ?>
									<div class="social-link">
										<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
											<g stroke="#FFD200" stroke-width="2" fill="none">
												<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
												<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
												<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
											</g>
										</svg>
									</div>
								<?php endif;
							endwhile; ?>
						<?php } ?>
						<?php if ( get_field('email') ) { ?>
							<a target="_blank" href="mailto:<?php the_field('email'); ?>" class="social-link">
								<svg xmlns="http://www.w3.org/2000/svg" width="69" height="54" viewBox="0 0 69 54">
									<defs>
										<linearGradient id="g" x1="452.07" x2="500.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
											<stop offset="0" stop-color="#FD7315"></stop>
											<stop offset="1" stop-color="#FFD200"></stop>
										</linearGradient>
									</defs>
									<path fill="url(#g)" d="M509.73 1007.4v30.45c0 1.71-.6 3.15-1.8 4.35a5.91 5.91 0 0 1-4.35 1.8h-56.43c-1.71 0-3.15-.6-4.35-1.8a5.91 5.91 0 0 1-1.8-4.35v-30.45a21.93 21.93 0 0 0 3.87 3.36 644.43 644.43 0 0 1 19.05 13.23c1.47 1.05 2.64 1.89 3.57 2.49.9.6 2.1 1.23 3.63 1.86 1.5.63 2.91.93 4.2.93h.09c1.29 0 2.7-.3 4.2-.93a21.1 21.1 0 0 0 3.63-1.86c.9-.6 2.1-1.44 3.54-2.49a813.1 813.1 0 0 1 19.11-13.23 22.03 22.03 0 0 0 3.84-3.36zm-1.83-15.6a5.9 5.9 0 0 0-4.32-1.8h-56.43c-1.98 0-3.51.66-4.56 1.98a7.88 7.88 0 0 0-1.59 5.01c0 1.59.69 3.36 2.1 5.22a19.03 19.03 0 0 0 4.5 4.44c.87.6 3.48 2.43 7.86 5.46 4.38 3.03 7.71 5.37 10.05 6.99.24.18.81.57 1.62 1.17.84.6 1.53 1.11 2.07 1.47.57.36 1.23.78 2.01 1.23.78.48 1.5.81 2.19 1.05s1.35.33 1.92.33h.09c.57 0 1.23-.09 1.92-.33.69-.24 1.41-.57 2.19-1.05.78-.45 1.44-.87 2.01-1.23.54-.36 1.23-.87 2.07-1.47.81-.6 1.35-.99 1.62-1.17l17.94-12.45a17.9 17.9 0 0 0 4.68-4.71 10.16 10.16 0 0 0 1.89-5.79c0-1.71-.6-3.15-1.83-4.35z" transform="translate(-441 -990)"></path>
								</svg>
							</a> 
						<?php } ?>
						<?php if( have_rows('topics') ) {
							while ( have_rows('topics') ) : the_row();
								if ( get_sub_field('year') == ($activeyear - 1) && get_sub_field('video') ) : ?>
									<div class="lightbox">
										<div class="contents">
											<?php the_sub_field('video'); ?>
										</div>
									</div>
								<?php endif;
							endwhile;
						} ?>
					</div>
				</article>
			<?php } ?>
		</section>
	<?php } ?>
<?php } ?>
<?php wp_reset_postdata(); ?>