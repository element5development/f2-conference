<a href="<?php the_permalink(); ?>" id="post-<?php echo get_the_ID(); ?>" class="post-search post-preview cta-card">
	<h2 class="entry-title"><?php the_title(); ?></h2>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div>
	<div class="btn btn--ghost"><span>Learn more</span></div>
</a>
