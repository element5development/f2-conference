<?php //DETERMINE YEAR TO DISPLAY
	$activeyear = get_field('active_year', 'options');
?>
<?php $args = array(
	'post_type' => array('venue'),
	'posts_per_page' => 1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'order' => 'DESC',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
);
$venue = new WP_Query( $args ); ?>
<?php if ( $venue->have_posts() ) { ?>
	<section class="venue-container">
		<?php while ( $venue->have_posts() ) { $venue->the_post(); ?>
			<article class="post-venue post-preview">
				<?php $icon = get_field('icon'); ?>
				<img class="icon" src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt']; ?>" />
				<?php $logo = get_field('logo'); ?>
				<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
				<?php the_field('description'); ?>
				<div class="block">
					<div class="block">
						<h2><?php the_field('location_headline'); ?></h2>
						<p>
							<b><?php the_field('location_name'); ?></b><br/>
							<?php the_field('address'); ?><br/>
							<?php the_field('city'); ?>, <?php the_field('state'); ?> <?php the_field('zip'); ?><br/>
						</p>
						<a target="_blank"  href="https://maps.google.com/?q=<?php the_field('address'); ?>,<?php the_field('city'); ?>, <?php the_field('state'); ?>, <?php the_field('zip'); ?>">
							Map & Directions
							<svg xmlns="http://www.w3.org/2000/svg" width="9" height="14" viewBox="0 0 9 14">
								<path d="M8.36 7.87l-6.43 5.29c-.56.45-1.32.3-1.71-.34a1.65 1.65 0 0 1-.22-.82v-10.58c0-.78.55-1.42 1.22-1.42.25 0 .5.09.71.26l6.43 5.29c.55.45.68 1.34.29 1.98a1.2 1.2 0 0 1-.29.34z"></path>
							</svg>
					</a>
					</div>
					<div class="block">
						<h2><?php the_field('group_rate_headline'); ?></h2>
						<?php the_field('group_rate'); ?>
					</div>
					<div class="block">
						<h2><?php the_field('amenities_headline'); ?></h2>
						<?php the_field('amenities'); ?>
					</div>
				</div>
				<?php if ( get_field('booking_url') ) { ?>
					<a target="_blank" href="<?php the_field('booking_url'); ?>" class="btn">Book now</a>
				<?php } ?>
				<?php if ( get_field('resort_website') ) { ?>
					<a target="_blank"  href="<?php the_field('resort_website'); ?>" class="btn btn--black">Visit resort website</a>
				<?php } ?>
			</article>
			<div class="gallery">
				<div class="block">
					<?php $images = get_field('venue_gallery');	?>
					<?php if( $images ): ?>
			      <?php foreach( $images as $image ): ?>
			          <div class="image-slide" style="background-image: url('<?php echo $image[url]; ?>')"></div>
			      <?php endforeach; ?>
					<?php endif; ?>
				</div>
				<nav>
					<?php 
						$images = get_field('venue_gallery');	
						$size = 'thumbnail';
					?>
					<?php if( $images ): ?>
			      <?php foreach( $images as $image ): ?>
			          <div class="nav-slide"><div class="image" style="background-image: url('<?php echo $image['sizes']['thumbnail']; ?>')"></div></div>
			      <?php endforeach; ?>
					<?php endif; ?>
				</nav>
			</div>
			<div class="city-info">
				<a target="_blank" href="<?php the_field('chamber_of_commerce'); ?>" class="cta-card">
					<h3><?php the_field('city'); ?><br/>Chamber of Commerce</h3>
					<div class="btn btn--ghost"><span>More information</span></div>
				</a>
				<a target="_blank" href="<?php the_field('visitors_bureau'); ?>" class="cta-card">
					<h3><?php the_field('city'); ?><br/>Visitors Bureau</h3>
					<div class="btn btn--ghost"><span>More information</span></div>
				</a>
				<a target="_blank" href="<?php the_field('weather'); ?>" class="cta-card">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="70" height="62" viewBox="0 0 70 62">
						<defs>
							<path id="a" d="M1098.75 2275.03c-2.26 0-4.37.56-6.28 1.48a19.62 19.62 0 1 0-17.85 27.73h24.13a14.6 14.6 0 1 0 0-29.21z"></path>
							<path id="b" d="M1082 2266.98a14.02 14.02 0 0 1 25.59 11.45"></path>
							<path id="c" d="M1075 2254l4.67 3.82"></path>
							<path id="d" d="M1095 2246v6.03"></path>
							<path id="e" d="M1113.63 2254l-4.63 3.87"></path>
							<path id="f" d="M1121.01 2271l-6.01.54"></path>
						</defs>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#a" fill="none" transform="translate(-1053 -2244)"></use>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#b" fill="none" transform="translate(-1053 -2244)"></use>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#c" fill="none" transform="translate(-1053 -2244)"></use>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#d" fill="none" transform="translate(-1053 -2244)"></use>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#e" fill="none" transform="translate(-1053 -2244)"></use>
						<use stroke="#FFD200" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="50" stroke-width="3" xlink:href="#f" fill="none" transform="translate(-1053 -2244)"></use>
					</svg>
					<h3><?php the_field('city'); ?><br/>Weather</h3>
				</a>
			</div>
		<?php } ?>
	</section>
<?php } else { ?>
	<section class="no-speakers no-content">
		<div class="block">
			<?php the_field('no_content'); ?>
		</div>
	</section>
	<?php $args = array(
		'post_type' => array('venue'),
		'posts_per_page' => 1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'order' => 'DESC',
		'tax_query' => array(
			array(
				'taxonomy' => 'conferenceyear',
				'field' => 'name',
				'terms' => array($activeyear - 1),
				'operator' => 'IN',
			),
		),
	);
	$venue = new WP_Query( $args ); ?>
	<?php if ( $venue->have_posts() ) { ?>
		<div class="headline-block">
			<h3><?php echo ($activeyear - 1); ?> Venue</h3>
		</div>
		<section class="venue-container">
		<?php while ( $venue->have_posts() ) { $venue->the_post(); ?>
			<article class="last-venue post-venue post-preview">
				<?php $logo = get_field('logo'); ?>
				<img class="logo" src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
			</article>
			<div class="gallery">
					<div class="block">
						<?php $images = get_field('venue_gallery');	?>
						<?php if( $images ): ?>
							<?php foreach( $images as $image ): ?>
									<div class="image-slide" style="background-image: url('<?php echo $image[url]; ?>')"></div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
					<nav>
						<?php 
							$images = get_field('venue_gallery');	
							$size = 'thumbnail';
						?>
						<?php if( $images ): ?>
							<?php foreach( $images as $image ): ?>
									<div class="nav-slide"><div class="image" style="background-image: url('<?php echo $image['sizes']['thumbnail']; ?>')"></div></div>
							<?php endforeach; ?>
						<?php endif; ?>
					</nav>
				</div>
		<?php } ?>
	</section>
	<?php } ?>
<?php } ?>
<?php wp_reset_postdata(); ?>