<?php if ( get_field('background_image') ) {
	$background = get_field('background_image'); 
} else {
	$background = get_field('default_post_image', 'options'); 
} ?>
<section class="title-page" style="background-image: url(<?php echo $background['url']; ?>)">
	<div class="title-overlay"></div>
	<div class="block">
		<h1><?php the_field('display_title'); ?></h1>
	</div>
	<div class="breadcrumbs">
		<nav>
			<?php yoast_breadcrumb(); ?>
		</nav>
	</div>
</section>
