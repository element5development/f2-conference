<?php 
	$currentID = get_the_ID();
	$currentCat = get_the_category();
	$args = array(
		'posts_per_page' => 3,
		'nopaging' => false,
		'ignore_sticky_posts' => true,
		'post__not_in' => array($currentID),
		'category_name' => $currentCat[0]->name,
	);
	$partner = new WP_Query( $args ); 
?>
<?php if ( $partner->have_posts() ) { ?>
	<h2 style="text-align: center;">Related Posts</h2>
	<section class="additional-posts">
		<?php while ( $partner->have_posts() ) { $partner->the_post(); ?>
			<?php 
				$posttCat = get_the_category();	
			?>
			<div class="post-contain post-preview cta-card fadeInScale animated">
				<a href="<?php the_permalink(); ?>">
					<div class="post-head">
						<div class="category-contain fadeInScale">
							<div class="category">
								<?php echo $postCat[0]->name; ?>
							</div>
						</div>
						<h2><?php the_title(); ?></h2>
					</div>
					<div class="post-content">
						<p><?php echo implode(' ', array_slice(explode(' ',get_the_excerpt()), 0, 25)); ?>[...]</p>
						<div class="btn">
							<span>Read the rest</span>
						</div>
					</div>
				</a>
			</div>
		<?php } ?>
	</section>
<?php } ?>
<?php wp_reset_postdata(); ?>