<div class="post-contain post-preview cta-card fadeInScale animated">
	<a href="<?php the_permalink(); ?>">

		<?php
			$categories = get_the_category();
			if ( ! empty( $categories ) ) {
				foreach( $categories as $category ) {
		?>

		<div class="category">
			<?php echo esc_html( $category->name ); ?>
		</div>
		
		<?php 
				}
			} 
		?>
		<div class="post-head">
			<h2><?php the_title(); ?></h2>
		</div>
		<div class="post-content">
			<?php the_excerpt(); ?>
			<div class="btn"><span>Read the rest</span></div>
		</div>
	</a>
</div>
