<header class="primary-nav-container">

	<?php if ( get_field('banner_link', 'options') ) : ?>
		<?php $bannerlink = get_field('banner_link', 'options');  ?>
		<div class="top-banner">
			<p><?php the_field('banner_text', 'options');  ?> <a href="<?php echo $bannerlink['url']; ?>" target="<?php echo $bannerlink['target']; ?>"><?php echo $bannerlink['title']; ?></a></p>
		</div>
	<?php endif; ?>

  <div class="block">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
      <img src="<?= get_template_directory_uri(); ?>/dist/images/F2F_Logo.svg" alt="Face 2 Face Entertainment Conference (F2FEC)" />
    </a>
    
    <nav class="nav-primary">
      <?php if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
      endif; ?>
    </nav>

    <!-- <a class="search">
      <svg xmlns="http://www.w3.org/2000/svg" width="92" height="92" viewBox="0 0 92 92">
        <path d="M63.68 38.92c0-6.8-2.4-12.64-7.24-17.48a23.8 23.8 0 0 0-17.52-7.28c-6.8 0-12.64 2.4-17.48 7.28a23.71 23.71 0 0 0-7.28 17.48 23.8 23.8 0 0 0 7.28 17.52 23.83 23.83 0 0 0 17.48 7.24c6.84 0 12.64-2.4 17.52-7.24a23.92 23.92 0 0 0 7.24-17.52zm28.32 46c0 1.92-.72 3.56-2.12 4.96a6.78 6.78 0 0 1-4.96 2.12c-2 0-3.64-.72-4.96-2.12l-18.96-18.88a37.96 37.96 0 0 1-22.08 6.84c-5.28 0-10.32-1-15.12-3.08a38.92 38.92 0 0 1-12.44-8.28 38.92 38.92 0 0 1-8.28-12.44 38.28 38.28 0 0 1-3.08-15.12c0-5.28 1.04-10.32 3.08-15.12 2.04-4.8 4.8-8.96 8.28-12.44a38.92 38.92 0 0 1 12.44-8.28 38.28 38.28 0 0 1 15.12-3.08c5.28 0 10.32 1.04 15.12 3.08 4.8 2.04 8.96 4.8 12.44 8.28a38.92 38.92 0 0 1 8.28 12.44 37.6 37.6 0 0 1 3.08 15.12c0 8.12-2.28 15.48-6.84 22.08l18.96 18.96a6.73 6.73 0 0 1 2.04 4.96z"></path>
      </svg>
    </a> -->

    <a class="hamburger">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30">
        <path d="M28.8 4.65v2.35a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.15v-2.35a1.14 1.14 0 0 1 .34-.81 1.1 1.1 0 0 1 .81-.34h25.3a1.16 1.16 0 0 1 1.15 1.15zM28.46 13a1.1 1.1 0 0 1 .34.81v2.3a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.11v-2.3a1.14 1.14 0 0 1 .34-.85 1.1 1.1 0 0 1 .81-.34h25.3a1.1 1.1 0 0 1 .81.34zM28.46 22.24a1.1 1.1 0 0 1 .34.81v2.3a1.1 1.1 0 0 1-.34.81 1.14 1.14 0 0 1-.81.34h-25.3a1.16 1.16 0 0 1-1.15-1.15v-2.35a1.14 1.14 0 0 1 .34-.81 1.1 1.1 0 0 1 .81-.34h25.3a1.1 1.1 0 0 1 .81.39z"></path>
      </svg>
    </a>

  </div>
</header>
<!-- 
REMOVED DUE TO THE LACK ON SEARCH ABLE CONTENT.
MAJORTIY OF PAGES ARE ACHIVES OF POSTS THAT DO NOT HAVE A SINGLE VIEW.

<div class="search-overlay">
  <div class="block">
    <?php get_search_form(); ?>
    <a class="close-search">
      <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42">
        <path fill-opacity=".5" d="M42 33.81c0 .93-.33 1.74-.99 2.4l-4.8 4.8c-.66.66-1.47.99-2.4.99a3.3 3.3 0 0 1-2.43-.99l-10.38-10.38-10.38 10.38a3.3 3.3 0 0 1-2.43.99c-.93 0-1.74-.33-2.4-.99l-4.8-4.8a3.28 3.28 0 0 1-.99-2.4c0-.96.33-1.77.99-2.43l10.38-10.38-10.38-10.38a3.3 3.3 0 0 1-.99-2.43c0-.93.33-1.74.99-2.4l4.8-4.8a3.28 3.28 0 0 1 2.4-.99c.96 0 1.77.33 2.43.99l10.38 10.38 10.38-10.38a3.3 3.3 0 0 1 2.43-.99c.93 0 1.74.33 2.4.99l4.8 4.8c.66.66.99 1.47.99 2.4a3.3 3.3 0 0 1-.99 2.43l-10.38 10.38 10.38 10.38c.66.66.99 1.47.99 2.43z"></path>
      </svg>
    </a>
  </div>
</div>
 -->
