<section class="faqs">
	<?php if( have_rows('faqs') ):
		while ( have_rows('faqs') ) : the_row(); ?>
		<?php $id = str_replace(' ', '-', strtolower(get_sub_field('section_id'))); ?>
		<div class="faq-contents editor-contents">
			<a id="<?php echo $id; ?>" class="anchor"></a>
			
			<?php if( have_rows('questions_answers') ):
				while ( have_rows('questions_answers') ) : the_row(); ?>

					<div class="single-faq hidden">
						<h3><?php the_sub_field('question'); ?></h3>
						<?php the_sub_field('answer'); ?>
					</div>

		    <?php endwhile;
			endif; ?>

		</div>
    <?php endwhile;
	endif; ?>
</section>
