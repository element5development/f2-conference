<?php 
	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC'
	) );
	$queried_object = get_queried_object(); 
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;  
?>
<?php if ( ! empty( $categories ) ) : ?>
	<nav class="cat-list">
		<ul>
			<?php foreach( $categories as $category ) : ?>
				<li>
					<?php 
						if ( $category->term_id == $term_id ) :
							$className = 'is-active';
						else :
							$className = '';
						endif; 
						if ( get_field('short_name', $taxonomy . '_' . $term_id) ) : 
							$catName = get_field('short_name', $taxonomy . '_' . $term_id); 
						else : 
							$catName = $category->name;
						endif;
					?>
					<a href="<?php echo get_category_link( $category->term_id ); ?>"  class="<?php echo $className; ?>">
						<?php echo $catName; ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</nav>
<?php endif; ?>

<section class="blog-grid-wrap">
	<div class="blog-grid-contain is-<?php the_field('list_vs_grid', $taxonomy . '_' . $term_id); ?>">
		<?php	while ( have_posts() ) : the_post(); ?>
			<a href="<?php the_permalink(); ?>" class="post-preview">
				<p><?php the_time('M - Y') ?></p>
				<h2><?php the_title(); ?></h2>
				<p><?php the_field('news_source'); ?></p>
				<div class="btn">Read More</div>
			</a>
		<?php endwhile; ?>
	</div>

</section>