<?php if ( get_field('display_form') == 'yes' && get_field('form') ) {
  get_template_part('templates/element/form');
} ?>

<?php if ( get_field('display_gallery') == 'yes' && get_field('gallery') ) {
  get_template_part('templates/element/gallery');
} ?>

<?php if ( get_field('display_testimonies') == 'yes' ) {
  get_template_part('templates/element/slider-testimony');
} ?>

<?php if ( get_field('display_cta_cards') == 'yes' && get_field('cta_cards') ) {
  get_template_part('templates/element/cta-cards');
} ?>

<?php if ( get_field('display_full_cta') == 'yes' && get_field('full_cta_heading') && get_field('full_cta_button_link') ) {
  get_template_part('templates/element/full-cta');
} ?>

<?php if ( is_page(286) ) { //CONTACT PAGE ?>
	<section class="media-container">
		<h2>Face to Face Returns in 2021</h2>
		<iframe src="https://www.youtube.com/embed/fdrKRTA7fK8?rel=0&amp;showinfo=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	</section>
<?php } ?>

<?php if ( get_field('display_partners') == 'yes' ) {
  get_template_part('templates/element/slider-partner');
} ?>

