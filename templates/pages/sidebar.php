<?php if ( have_rows('sidebar') ) { ?>
	<nav>
		<ul>
			<?php while ( have_rows('sidebar') ) : the_row(); ?>
				<?php $id = str_replace(' ', '-', strtolower(get_sub_field('id'))); ?>
				<li><a href="#<?php echo $id; ?>" class="smoothScroll"><?php the_sub_field('label'); ?></a></li>
			<?php endwhile; ?>
		</ul>
	</nav>
<?php } else {
	dynamic_sidebar('sidebar-primary');
} ?>


