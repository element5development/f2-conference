<?php 
	$categories = get_categories( array(
		'orderby' => 'name',
		'order'   => 'ASC'
	) );
?>

<section class="blog-categories">
	<?php if ( ! empty( $categories ) ) : ?>
		<?php  foreach( $categories as $category ) : ?>
			<?php $background = get_field('background_image', $category); ?>
			<a href="<?php echo get_category_link( $category->term_id ); ?>">
				<div class="category cta-card" style="background-image: url(<?php echo esc_url($background['url']); ?>);">
					<h2><?php echo $category->name; ?></h2>
					<p><?php echo $category->category_count; ?> Total Articles</p>
					<div class="btn">View Now</div>
				</div>
			</a>
		<?php endforeach; ?>
	<?php endif; ?>
</section>