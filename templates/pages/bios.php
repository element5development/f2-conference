<section class="bios">
	<div class="bio-headshots">
		<?php if( have_rows('bios') ):
			while ( have_rows('bios') ) : the_row(); $i++; ?>
				<?php $headshot = get_sub_field('headshot'); ?>
				<div class="headshot <?php echo $i; ?>">
					<img src="<?php echo $headshot['url']; ?>" alt="<?php echo $headshot['alt']; ?>" />
					<div class="overlay">
						<h2><?php the_sub_field('name'); ?></h2>
						<svg xmlns="http://www.w3.org/2000/svg" width="92" height="90" viewBox="0 0 92 90">
							<path stroke-miterlimit="50" stroke-width="6" d="M3 45c0-23.2 19.25-42 43-42s43 18.8 43 42-19.25 42-43 42-43-18.8-43-42z"></path>
							<path stroke-linecap="square" stroke-miterlimit="50" stroke-width="4" d="M44.36 29.68v29.74m16.38-14.42h-29.74"></path>
						</svg>
					</div>
				</div>
			<?php endwhile;
		endif; ?>
	</div>
	<div class="bio-summary">
		<?php if( have_rows('bios') ): $i = 0;
			while ( have_rows('bios') ) : the_row(); $i++; ?>
				<div class="editor-contents <?php echo $i; ?>">
					<h2><?php the_sub_field('name'); ?></h2>
					<?php the_sub_field('bio'); ?>
				</div>
			<?php endwhile;
		endif; ?>
	</div>
</section>
