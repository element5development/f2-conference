<?php $background = get_field('editor_bg_image'); ?>
<section class="editor-bg" data-parallax="scroll" data-image-src="<?php echo $background['url']; ?>" data-speed=".5">
	<div class="editor-contents">
		<?php the_field('editor_bg_contents'); ?>
	</div>
</section>