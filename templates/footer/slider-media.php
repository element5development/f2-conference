<?php 
	if ( date('n') > 2 ) {
		$activeyear = date('Y') + 1;
	} else {
		$activeyear = date('Y');
	}
	$args = array(
		'post_type' => array('partner'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'orderby'=> 'title',
		'order' => 'ASC',
		'meta_key' => 'media',
		'meta_value' => 'yes',
		'tax_query' => array(
			array(
				'taxonomy' => 'conferenceyear',
				'field' => 'name',
				'terms' => array($activeyear),
				'operator' => 'IN',
			),
		),
	);
	$partners = new WP_Query( $args );
?>
<?php if ( $partners->have_posts() ) { ?>
	<section class="media-container">
		<h2>Our Media Partners</h2>
		<div class="block">
			<?php while ( $partners->have_posts() ) { $partners->the_post(); ?>
				<div class="media-slide">
					<?php $logo = get_field('logo'); ?>
					<a target="_blank" href="<?php the_field('website'); ?>">
						<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
					</a>
				</div>
			<?php } ?>
		</div>
	</section>
<?php } else { ?>
	<?php
		$args = array(
			'post_type' => array('partner'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'ignore_sticky_posts' => true,
			'orderby'=> 'title',
			'order' => 'ASC',
			'meta_key' => 'media',
			'meta_value' => 'yes',
			'tax_query' => array(
				array(
					'taxonomy' => 'conferenceyear',
					'field' => 'name',
					'terms' => array($activeyear -1),
					'operator' => 'IN',
				),
			),
		);
		$partners = new WP_Query( $args );
	?>
	<?php if ( $partners->have_posts() ) { ?>
		<section class="media-container">
			<h2>Our Media Partners</h2>
			<div class="block">
				<?php while ( $partners->have_posts() ) { $partners->the_post(); ?>
					<div class="media-slide">
						<?php $logo = get_field('logo'); ?>
						<a target="_blank" href="<?php the_field('website'); ?>">
							<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
						</a>
					</div>
				<?php } ?>
			</div>
		</section>
	<?php } ?>
<?php }
wp_reset_postdata(); ?>