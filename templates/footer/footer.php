<?php if ( !is_page_template( 'template-styleguide.php' ) && !is_single() && !is_404() || !is_search() ) {
	get_template_part('templates/footer/slider-media'); 
} ?>

<?php if ( !is_page_template( 'template-recap.php' ) && !is_front_page() && !is_single() ) {
	get_template_part('templates/footer/theme-invite'); 
} ?>

<footer>
	<div class="block">
		<nav class="nav-footer">
			<?php if (has_nav_menu('footer_navigation')) :
				wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']);
			endif; ?>
		</nav>
		<div class="left">
			<p>"Be In The Room" , "Experience. Passion. Opportunity" , "Soul is the humanity of your organization" and “RISE. Rest. Rebuild Reach” are registered trademarks of F2FEC, LLC. “Share. Engage. Grow.” is used with permission and is a registered trademark of SMASH-Life, LLC.</p>
			<p>Conference support provided by <a target="_blank" href="http://sealymedia.com/">Sealy Media</a> (photography), <a target="_blank" href="http://chaffincreative.com/">Natalia Chaffin CREATIVE</a> (scripting and video), <a target="_blank" href="https://www.attackdogmedia.com/">Attack Dog Media</a> (video content), <a target="_blank" href="https://www.jloto.com/">Jon Lotoczky</a> (video content), and <a target="_blank" href="https://mellerperformanceevents.com/">Miller Performance Events</a> (event management).</p>
			<p>© Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ) ?>. All Rights Reserved.</p> 
		</div>
		<div class="middle">
			<p>F2FEC HEADQUARTERS</p>
			<?php 
				$phone = get_field('primary_phone','options');
				$phone = preg_replace('/[^0-9]/', '', $phone);
				$phone = '+1' . $phone;
			?>
			Phone: <a href="tel:<?php echo $phone;?>"><?php the_field('primary_phone', 'options'); ?></a>
			Address: <a href="https://www.google.com/maps/place/<?php the_field('primary_address', 'options'); ?>,+<?php the_field('primary_city', 'options'); ?>,+<?php the_field('primary_state', 'options'); ?>+<?php the_field('primary_zip', 'options'); ?>"><?php the_field('primary_address', 'options'); ?><br/><?php the_field('primary_city', 'options'); ?>, <?php the_field('primary_state', 'options'); ?> <?php the_field('primary_zip', 'options'); ?></a>
		</div>
		<div class="right">
			<p>Conference Coordination Team:</p>
			<nav class="nav-coordination">
			  <?php if (has_nav_menu('coordination_navigation')) :
			    wp_nav_menu(['theme_location' => 'coordination_navigation', 'menu_class' => 'nav']);
			  endif; ?>
			</nav>
		</div>
	</div>
</footer>

<div class="past-container">
	<div class="past-wrapper">
		<h4>Check out past experiences</h4>
		<nav class="nav-past">
			<?php if (has_nav_menu('primary_navigation')) :
				wp_nav_menu(['theme_location' => 'past_navigation', 'menu_class' => 'nav']);
			endif; ?>
		</nav>
	</div>
</div>