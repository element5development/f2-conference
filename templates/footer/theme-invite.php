<section class="theme-invite">
	<?php $background = get_field('theme_image', 'options'); ?>
	<div class="block event-1" style="background-image: url(<?php echo $background['url']; ?>);">
		<h3><?php the_field('event_title', 'options'); ?></h3>
		<p><?php the_field('event_info', 'options'); ?></p>
	</div>
	<div class="block">
		<div class="invite">
			<h2>Be in the Room.<span>&trade;<span></h2>
			<p>The value of showing up to Be in the Room™ at an F2F Experience is that everyone has something to share and everyone has something to gain. WE are better than me, and WE are better together! We are all going to participate in events relevant to our needs, our challenges, our interests and our future. Join us!</p>
			<a href="<?php get_site_url(); ?>/contact/?reason=attend" class="btn">Request to Be in the Room&trade;</a>
		</div>
	</div>
	<?php $background2 = get_field('theme_image_2', 'options'); ?>
	<div class="block event-2" style="background-image: url(<?php echo $background2['url']; ?>);">
		<h3><?php the_field('event_title_2', 'options'); ?></h3>
		<p><?php the_field('event_info_2', 'options'); ?></p>
	</div>
</section>