<?php 
/*-------------------------------------------------------------------
    Template Name: Agenda
-------------------------------------------------------------------*/
?>

<?php //DETERMINE YEAR TO DISPLAY
	$activeyear = get_field('active_year', 'options');
	if ( get_field('day_one_date') ) {
		$dateYear = get_field('day_one_date');
		$dateYear = new DateTime($dateYear);
		$dateYear = $dateYear->format('Y');
	} else {
		$dateYear = 2999;
	}
?>

<?php get_template_part('templates/pages/title'); ?>

<?php if ( $dateYear == $activeyear ) { ?>
	<?php if(!empty(get_the_content())) : ?>
		<section class="editor-contents">
			<?php get_template_part('templates/pages/default-contents'); ?>
		</section>
	<?php endif; ?>
	<!--DAY ONE-->
	<section class="agenda-day">
		<div class="left">
			<?php 
				$date = get_field('day_one_date');
				$dateTime = DateTime::createFromFormat("l, F d, Y", $date);
				if ( is_object($dateTime) ) {
					$month = $dateTime->format('F');
					$date = $dateTime->format('d');
				}
				$month = substr($month, 0, 3);
			?>
			<div class="block sticky-date">
				<div class="label">Day 1</div>
				<div class="left-month"><?php echo $month; ?></div>
				<div class="left-date"><?php echo $date; ?></div>
			</div>
		</div>
		<div class="right">
			<h2>Day 1</h2>
			<h3><?php the_field('day_one_date'); ?></h3>
			<?php $args = array(
				'post_type' => array('days'),
				'posts_per_page' => -1,
				'nopaging' => true,
				'ignore_sticky_posts' => true,
				'meta_key'			=> 'time',
				'orderby'			=> 'meta_value',
				'order'				=> 'ASC',
				'meta_query'	=> array(
					'relation'		=> 'AND',
					array(
						'key'		=> 'day',
						'value'		=> 'one',
						'compare'	=> '='
					),
				),
				'tax_query' => array(
					array(
						'taxonomy' => 'conferenceyear',
						'field' => 'name',
						'terms' => array($activeyear),
						'operator' => 'IN',
					),
				),
			);
			$days = new WP_Query( $args ); ?>
			<?php if ( $days->have_posts() ) : ?>
				<div class="day-post-container">
					<?php while ( $days->have_posts() ) : $days->the_post(); ?>
						<?php if ( get_field('format') !== "basic" ) : ?>
							<article class="day-preview post-preview dark">
						<?php else : ?>
							<article class="day-preview post-preview">
						<?php endif; ?>
							<div class="block">
								<p><?php the_field('time'); ?></p>
								<?php if ( get_field('format') !== "basic" ) : ?>
									<div class="label"><?php the_field('format'); ?></div>
								<?php endif; ?>
							</div>
							<div class="block">
								<h4><?php the_field('title'); ?></h4>
								<p><?php the_field('summary') ?></p>
							</div>
						</article>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</section>
	<!--DAY TWO-->
	<section class="agenda-day">
		<div class="left">
			<?php 
				$date = get_field('day_two_date');
				$dateTime = DateTime::createFromFormat("l, F d, Y", $date);
				if ( is_object($dateTime) ) {
					$month = $dateTime->format('F');
					$date = $dateTime->format('d');
				}
				$month = substr($month, 0, 3);
			?>
			<div class="block sticky-date">
				<div class="label">Day 2</div>
				<div class="left-month"><?php echo $month; ?></div>
				<div class="left-date"><?php echo $date; ?></div>
			</div>
		</div>
		<div class="right">
			<h2>Day 2</h2>
			<h3><?php the_field('day_two_date'); ?></h3>
			<?php $args = array(
				'post_type' => array('days'),
				'posts_per_page' => -1,
				'nopaging' => true,
				'ignore_sticky_posts' => true,
				'meta_key'			=> 'time',
				'orderby'			=> 'meta_value',
				'order'				=> 'ASC',
				'meta_query'	=> array(
					'relation'		=> 'AND',
					array(
						'key'		=> 'day',
						'value'		=> 'two',
						'compare'	=> '='
					),
				),
				'tax_query' => array(
					array(
						'taxonomy' => 'conferenceyear',
						'field' => 'name',
						'terms' => array($activeyear),
						'operator' => 'IN',
					),
				),
			);
			$days = new WP_Query( $args ); ?>
			<?php if ( $days->have_posts() ) : ?>
				<div class="day-post-container">
					<?php while ( $days->have_posts() ) : $days->the_post(); ?>
					<?php if ( get_field('format') !== "basic" ) : ?>
						<article class="day-preview post-preview dark">
					<?php else : ?>
						<article class="day-preview post-preview">
					<?php endif; ?>
						<div class="block">
							<p><?php the_field('time'); ?></p>
							<?php if ( get_field('format') !== "basic" ) : ?>
								<div class="label"><?php the_field('format'); ?></div>
							<?php endif; ?>
						</div>
							<div class="block">
								<h4><?php the_field('title'); ?></h4>
								<p><?php the_field('summary') ?></p>
							</div>
						</article>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</section>
	<!--DAY THREE-->
	<section class="agenda-day">
		<div class="left">
			<?php 
				$date = get_field('day_three_date');
				$dateTime = DateTime::createFromFormat("l, F d, Y", $date);
				if ( is_object($dateTime) ) {
					$month = $dateTime->format('F');
					$date = $dateTime->format('d');
				}
				$month = substr($month, 0, 3);
			?>
			<div class="block sticky-date">
				<div class="label">Day 3</div>
				<div class="left-month"><?php echo $month; ?></div>
				<div class="left-date"><?php echo $date; ?></div>
			</div>
		</div>
		<div class="right">
			<h2>Day 3</h2>
			<h3><?php the_field('day_three_date'); ?></h3>
			<?php $args = array(
				'post_type' => array('days'),
				'posts_per_page' => -1,
				'nopaging' => true,
				'ignore_sticky_posts' => true,
				'meta_key'			=> 'time',
				'orderby'			=> 'meta_value',
				'order'				=> 'ASC',
				'meta_query'	=> array(
					'relation'		=> 'AND',
					array(
						'key'		=> 'day',
						'value'		=> 'three',
						'compare'	=> '='
					),
				),
				'tax_query' => array(
					array(
						'taxonomy' => 'conferenceyear',
						'field' => 'name',
						'terms' => array($activeyear),
						'operator' => 'IN',
					),
				),
			);
			$days = new WP_Query( $args ); ?>
			<?php if ( $days->have_posts() ) : ?>
				<div class="day-post-container">
					<?php while ( $days->have_posts() ) : $days->the_post(); ?>
					<?php if ( get_field('format') !== "basic" ) : ?>
						<article class="day-preview post-preview dark">
					<?php else : ?>
						<article class="day-preview post-preview">
					<?php endif; ?>
						<div class="block">
							<p><?php the_field('time'); ?></p>
							<?php if ( get_field('format') !== "basic" ) : ?>
								<div class="label"><?php the_field('format'); ?></div>
							<?php endif; ?>
						</div>
							<div class="block">
								<h4><?php the_field('title'); ?></h4>
								<p><?php the_field('summary') ?></p>
							</div>
						</article>
					<?php endwhile; ?>
				</div>
			<?php endif; ?>
			<?php wp_reset_postdata(); ?>
		</div>
	</section>
<?php } else { ?>
	<section class="no-speakers no-content">
		<div class="block">
			<?php the_field('no_content'); ?>
		</div>
	</section>
	<?php $background = get_field('default_page_image', 'options');  ?>
<?php } ?>

<?php get_template_part('templates/pages/closing'); ?>