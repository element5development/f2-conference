<?php 
/*-------------------------------------------------------------------
		Template Name: Recap
-------------------------------------------------------------------*/
?>

<?php 
	$activeyear = get_field('active_year');
?>

<?php get_template_part('templates/pages/title'); ?>

<nav class="recap-nav">
	<li><a href="#recap" class="smoothScroll" title="Recap">Recap</a></li>
	<li><a href="#venue" class="smoothScroll" title="Venue">Venue</a></li>
	<li><a href="#speakers" class="smoothScroll" title="Speakers">Speakers</a></li>
	<li><a href="#partners" class="smoothScroll" title="Partners">Partners</a></li>
	<li><a href="#support" class="smoothScroll" title="Support Team">Support Team</a></li>
</nav>

<?php if(!empty(get_the_content())) { ?>
	<section class="editor-contents">
		<a id="recap" class="anchor"></a>
		<?php get_template_part('templates/pages/default-contents'); ?>
		<?php if ( get_field('title_video') ) { ?>
			<a target="_blank" class="btn btn--video"><span>Watch the 2017 experience</span></a>
		<?php } ?>
		<?php if ( get_field('recap_photos') ) { ?>
			<a target="_blank" href="<?php the_field('recap_photos'); ?>" class="btn btn--black"><span>See More Photos</span></a>
		<?php } ?>
		<?php if ( get_field('directory_request') ) { ?>
			<a href="<?php the_field('directory_request'); ?>" class="btn btn--black"><span>Download Directory</span></a>
		<?php } ?>
	</section>
<?php } ?>

<?php if ( get_field('gallery') ) { ?>
	<section class="gallery">
		<div class="block">
			<?php $images = get_field('gallery');	?>
			<?php if( $images ): ?>
				<?php foreach( $images as $image ): ?>
						<div class="image-slide" style="background-image: url('<?php echo $image[url]; ?>')"></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		<nav>
			<?php 
				$images = get_field('gallery');	
				$size = 'thumbnail';
			?>
			<?php if( $images ): ?>
				<?php foreach( $images as $image ): ?>
						<div class="nav-slide"><div class="image" style="background-image: url('<?php echo $image['sizes']['thumbnail']; ?>')"></div></div>
				<?php endforeach; ?>
			<?php endif; ?>
		</nav>
	</section>
<?php } ?>

<?php $args = array(
	'post_type' => array('venue'),
	'posts_per_page' => 1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'order' => 'DESC',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
); 
$venue = new WP_Query( $args ); ?>
<?php if ( $venue->have_posts() ) { ?>
	<section class="venue-recap">
	<a id="venue" class="anchor"></a>
		<?php while ( $venue->have_posts() ) { $venue->the_post(); ?>
			<div class="block">
				<div class="left">
					<h2><?php echo $activeyear; ?> Venue</h2>
					<h3><?php the_field('city'); ?>, <?php the_field('state'); ?></h3>
				</div>
				<div class="right">
					<h3><?php the_field('location_name'); ?></h3>
					<?php the_field('description'); ?>
				</div>
			</div>
			<div class="image-preview">
				<?php $logo = get_field('logo'); ?>
				<div class="logo" style="background-image: url(<?php echo $logo['url']; ?>)"></div>
				<?php $images = get_field('venue_gallery');	?>
				<?php if( $images ): $i = 0; ?>
					<?php foreach( $images as $image ): $i++; ?>
						<?php if ( $i < 2) { ?>
							<div class="image-slide" style="background-image: url('<?php echo $image[url]; ?>')"></div>
						<?php } else {} ?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		<?php } ?>
	</section>
<?php }	?>
<?php wp_reset_postdata(); ?>

<?php $args = array(
	'post_type' => array('speaker'),
	'posts_per_page' => -1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'meta_key'	=> 'last_name',
	'orderby'	=> 'meta_value',
	'order' => 'ASC',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
);
$speakers = new WP_Query( $args ); ?>
<?php if ( $speakers->have_posts() ) { ?>
	<h2 class="section-title"><?php echo $activeyear; ?> Speakers</h2>
	<p>To view the presenter’s talk click on the film icon below each speakers picture.</p>
	<a id="speakers" class="anchor" style="position: relative;"></a>
	<section class="speakers-container">
		<?php while ( $speakers->have_posts() ) { $speakers->the_post(); ?>
			<article class="post-speaker post-preview cta-card hidden">
				<?php 
					$headshot = get_field('headshot'); 
					$headshotURL = $headshot['sizes']['medium'];
				?>
				<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-headshot" style="background-image: url('<?php echo $headshotURL; ?>');"></a>
				<a target="_blank" href="<?php the_field('company_website'); ?>">
					<h2 class="speaker-name"><?php the_field('name'); ?> <?php the_field('last_name'); ?></h2>
				</a>
				<a target="_blank" href="<?php the_field('company_website'); ?>" class="speaker-company">
					<h3><?php the_field('company'); ?></h3>
				</a>
				<p class="speaker-intro">
					<?php if( have_rows('topics') ) :
						while ( have_rows('topics') ) : the_row();
							if ( get_sub_field('year') == $activeyear ) :
								the_sub_field('topic');
							endif;
						endwhile;
					endif; ?>
				</p>
				<div class="speaker-contact">
					<?php if ( get_field('linkedin') ) { ?>
						<a target="_blank" href="<?php the_field('linkedin') ?>" class="social-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="54" height="54" viewBox="0 0 54 54">
								<defs>
									<linearGradient id="f" x1="158.66" x2="196.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
										<stop offset="0" stop-color="#FD7315"></stop>
										<stop offset="1" stop-color="#FFD200"></stop>
									</linearGradient>
								</defs>
								<path fill="url(#f)" d="M158.34 1010.82h8.1v24.39h-8.1zm7.38-4.53a4.6 4.6 0 0 1-3.33 1.23h-.06c-1.32 0-2.4-.42-3.24-1.23a3.99 3.99 0 0 1-1.26-3c0-1.23.45-2.22 1.29-3.03a4.82 4.82 0 0 1 3.33-1.2c1.35 0 2.43.42 3.27 1.2.81.81 1.23 1.8 1.26 3.03 0 1.2-.42 2.19-1.26 3zm21.84 15.87c0-3.66-1.38-5.52-4.08-5.52-1.05 0-1.92.3-2.61.87a5.22 5.22 0 0 0-1.59 2.1 6.18 6.18 0 0 0-.24 1.95v13.65h-8.13c.09-14.73.09-22.86 0-24.39h8.13v3.45a8.16 8.16 0 0 1 7.29-4.02 8.8 8.8 0 0 1 6.78 2.79c1.71 1.83 2.55 4.56 2.55 8.19v13.98h-8.1zm13.47-29.19a9.77 9.77 0 0 0-7.14-2.97h-33.78c-2.76 0-5.16.99-7.14 2.97a9.77 9.77 0 0 0-2.97 7.14v33.78c0 2.76.99 5.16 2.97 7.14a9.77 9.77 0 0 0 7.14 2.97h33.78c2.76 0 5.16-.99 7.14-2.97a9.77 9.77 0 0 0 2.97-7.14v-33.78c0-2.76-.99-5.16-2.97-7.14z" transform="translate(-150 -990)"></path>
							</svg>
						</a>
					<?php } ?>
					<?php if( have_rows('topics') ) {
						while ( have_rows('topics') ) : the_row();
							if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
								<div class="social-link">
									<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
										<g stroke="#FFD200" stroke-width="2" fill="none">
											<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
											<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
											<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
										</g>
									</svg>
								</div>
							<?php endif;
						endwhile; ?>
					<?php } ?>
					<?php if ( get_field('email') ) { ?>
						<a target="_blank" href="mailto:<?php the_field('email'); ?>" class="social-link">
							<svg xmlns="http://www.w3.org/2000/svg" width="69" height="54" viewBox="0 0 69 54">
								<defs>
									<linearGradient id="g" x1="452.07" x2="500.8" y1="990" y2="1035.4" gradientUnits="userSpaceOnUse">
										<stop offset="0" stop-color="#FD7315"></stop>
										<stop offset="1" stop-color="#FFD200"></stop>
									</linearGradient>
								</defs>
								<path fill="url(#g)" d="M509.73 1007.4v30.45c0 1.71-.6 3.15-1.8 4.35a5.91 5.91 0 0 1-4.35 1.8h-56.43c-1.71 0-3.15-.6-4.35-1.8a5.91 5.91 0 0 1-1.8-4.35v-30.45a21.93 21.93 0 0 0 3.87 3.36 644.43 644.43 0 0 1 19.05 13.23c1.47 1.05 2.64 1.89 3.57 2.49.9.6 2.1 1.23 3.63 1.86 1.5.63 2.91.93 4.2.93h.09c1.29 0 2.7-.3 4.2-.93a21.1 21.1 0 0 0 3.63-1.86c.9-.6 2.1-1.44 3.54-2.49a813.1 813.1 0 0 1 19.11-13.23 22.03 22.03 0 0 0 3.84-3.36zm-1.83-15.6a5.9 5.9 0 0 0-4.32-1.8h-56.43c-1.98 0-3.51.66-4.56 1.98a7.88 7.88 0 0 0-1.59 5.01c0 1.59.69 3.36 2.1 5.22a19.03 19.03 0 0 0 4.5 4.44c.87.6 3.48 2.43 7.86 5.46 4.38 3.03 7.71 5.37 10.05 6.99.24.18.81.57 1.62 1.17.84.6 1.53 1.11 2.07 1.47.57.36 1.23.78 2.01 1.23.78.48 1.5.81 2.19 1.05s1.35.33 1.92.33h.09c.57 0 1.23-.09 1.92-.33.69-.24 1.41-.57 2.19-1.05.78-.45 1.44-.87 2.01-1.23.54-.36 1.23-.87 2.07-1.47.81-.6 1.35-.99 1.62-1.17l17.94-12.45a17.9 17.9 0 0 0 4.68-4.71 10.16 10.16 0 0 0 1.89-5.79c0-1.71-.6-3.15-1.83-4.35z" transform="translate(-441 -990)"></path>
							</svg>
						</a> 
					<?php } ?>
					<?php if( have_rows('topics') ) {
						while ( have_rows('topics') ) : the_row();
							if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
								<div class="lightbox">
									<div class="contents">
										<?php the_sub_field('video'); ?>
									</div>
								</div>
							<?php endif;
						endwhile;
					} ?>
				</div>
			</article>
		<?php } ?>
	</section>
<?php }	?>
<?php wp_reset_postdata(); ?>

<?php $args = array(
	'post_type' => array('partner'),
	'posts_per_page' => -1,
	'nopaging' => true,
	'ignore_sticky_posts' => true,
	'orderby'=> 'title',
	'order' => 'ASC',
	'meta_key' => 'media',
	'meta_value' => 'no',
	'tax_query' => array(
		array(
			'taxonomy' => 'conferenceyear',
			'field' => 'name',
			'terms' => array($activeyear),
			'operator' => 'IN',
		),
	),
);
$partner = new WP_Query( $args ); ?>
<?php if ( $partner->have_posts() ) { ?>
	<hr>
	<h2 class="section-title"><?php echo $activeyear; ?> Partners</h2>
	<p>Please view partner’s Showcase video by clicking on the film icon below each company’s logo.</p>
	<a id="partners" class="anchor" style="position: relative;"></a>
	<section class="partners-container">
		<?php while ( $partner->have_posts() ) { $partner->the_post(); ?>
			<article class="post-partner post-preview cta-card hidden">
				<?php 
					$logo = get_field('logo'); 
					$logoURL = $logo['sizes']['medium'];
				?>
				<a target="_blank" href="<?php the_field('website'); ?>" class="partner-logo">
					<img src="<?php echo $logoURL; ?>" alt="<?php echo $logo['alt'] ?>" />
				</a>
				<p class="partner-intro"><?php the_field('description'); ?></p>
				<p class="partner-contact"><?php the_field('representative_names'); ?></p>
				<div class="partner-video">
					<?php if( have_rows('videos') ) {
						while ( have_rows('videos') ) : the_row();
							if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
								<svg width="50" height="30" viewBox="0 0 50 30" xmlns="http://www.w3.org/2000/svg" class="speaker-video">
									<g stroke="#FFD200" stroke-width="2" fill="none">
										<path id="left" d="M32.055 1.026h-31.028v.011l.063-.008c2.621 0 4.748 2.127 4.748 4.748 0 2.621-2.127 4.748-4.748 4.748 2.621 0 4.748 2.124 4.748 4.746 0 2.623-2.127 4.746-4.748 4.746 2.621 0 4.748 2.125 4.748 4.748 0 2.621-2.127 4.746-4.748 4.746l-.063-.006v.011h31.028"></path>
										<path id="right" d="M19.035 1.026h30.144l.886.011-.065-.008c-2.621 0-4.746 2.127-4.746 4.748 0 2.621 2.125 4.748 4.746 4.748-2.621 0-4.746 2.124-4.746 4.746 0 2.623 2.125 4.746 4.746 4.746-2.621 0-4.746 2.125-4.746 4.748 0 2.621 2.125 4.746 4.746 4.746l.065-.006v.011h-31.029"></path>
										<path id="play" d="M20.208 22.873l13.216-6.677c.279-.141.279-.494 0-.635l-13.216-6.677c-.278-.141-.627.036-.627.317v13.355c0 .283.349.458.627.317z"></path>
									</g>
								</svg>
							<?php endif;
						endwhile; ?>
					<?php } ?>
				</div> 
				<?php if( have_rows('videos') ) {
					while ( have_rows('videos') ) : the_row();
						if ( get_sub_field('year') == $activeyear && get_sub_field('video') ) : ?>
							<div class="lightbox">
								<div class="contents">
									<?php the_sub_field('video'); ?>
								</div>
							</div>
						<?php endif;
					endwhile;
				} ?>
			</article>
		<?php } ?>
	</section>
<?php }	?>
<?php wp_reset_postdata(); ?>
<?php if ( get_field('thanks') ) { ?>
	<h2 class="section-title">A Special Thanks</h2>
	<section class="thank-yous editor-contents">
	<a id="support" class="anchor"></a>
		<?php 
			$i = 0;
			$rows = get_field('thanks');
			echo '<div class="odd-column">' ;
				foreach($rows as $row) {
					if (($i++ % 2) == 0 ) { 
						if ( $row['company'] ) {
							echo '<p class="hidden">' . $row['name']  . '<br/>'. $row['company'] .'<br/>' . $row['message'] . '</p>';
						} else {
							echo '<p class="hidden">' . $row['name']  . '<br/>' . $row['message'] . '</p>';
						}
					}
				}
			echo '</div>' ;
			$i = 0; rewind_posts();
			$rows = get_field('thanks');
			echo '<div class="even-column">' ;
				foreach($rows as $row) {
					if (($i++ % 2) != 0 ) { 
						if ( $row['company'] ) {
							echo '<p class="hidden">' . $row['name']  . '<br/>'. $row['company'] .'<br/>' . $row['message'] . '</p>';
						} else {
							echo '<p class="hidden">' . $row['name']  . '<br/>' . $row['message'] . '</p>';
						}
					}
				}
			echo '</div>' ;
		?>
	</section>
<?php } ?>
<?php get_template_part('templates/pages/closing'); ?>