<form action="<?= esc_url(home_url('/')); ?>" method="get">
   <ul>
    <li class="gfield">
    	<label for="search">Search for...</label>
    	<input type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
    </li>
   </ul>
</form>